# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/10/28 22:27:26 by pmarquis          #+#    #+#              #
#    Updated: 2022/11/25 01:26:41 by pmarquis         ###   lausanne.ch        #
#                                                                              #
# **************************************************************************** #

.PHONY: all clean fclean mrproper re tests bonus libft/libft.a

NAME = libftprintf.a

SRCDIR = src
LIBFTDIR = libft

CC := gcc
CFLAGS := -Wall -Wextra -Werror#-g -fsanitize=address

INC	= $(wildcard $(SRCDIR)/*.h)
SRC = $(wildcard $(SRCDIR)/*.c)
OBJ = $(patsubst %.c,%.o,$(SRC))

# Options
OPT_DEBUG ?= 0
export OPT_DEBUG

OPT_PREALLOC ?= 0
export OPT_PREALLOC

OPT_MEMLIST ?= 1
export OPT_MEMLIST

OPT_LINUX ?= 1
export OPT_LINUX
##

DEFINES =
ifeq ($(OPT_DEBUG),0)
DEFINES += -DNDEBUG
endif
ifeq ($(OPT_PREALLOC),1)
DEFINES += -DPREALLOC
endif
ifeq ($(OPT_MEMLIST),0)
DEFINES += -DNMEMLIST
endif
ifeq ($(OPT_LINUX),1)
DEFINES += -DLINUX
endif

INCPATH = -I$(LIBFTDIR)

.c.o:
	$(CC) -c $(CFLAGS) $(DEFINES) $(INCPATH) -o $@ $<

.DEFAULT_GOAL = all

all: $(NAME)

$(NAME): $(LIBFTDIR)/libft.a $(OBJ)
	cp -f $< $@
	ar rcs $@ $(OBJ)

$(LIBFTDIR)/libft.a:
	$(MAKE) -C $(LIBFTDIR)

clean:
	rm -f $(OBJ)
	$(MAKE) -C $(LIBFTDIR) clean

fclean: clean
	rm -f $(NAME)
	$(MAKE) -C $(LIBFTDIR) fclean

mrproper: fclean
	rm -f cscope.* tags
	$(MAKE) -C $(LIBFTDIR) mrproper
	$(MAKE) -C tests mrproper

re: fclean all

tests: $(NAME)
	$(MAKE) -C $@ leaks

bonus: $(NAME)

### deps
$(OBJ): $(INC)

