/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_tester.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/31 22:17:07 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/05 22:21:13 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include <ft_printf.h>


#ifdef NO_SHOW_OK
#define	SHOW_OK	0
#else
#define SHOW_OK	1
#endif

#ifdef NO_DESCRIBE_OK
#define DESCRIBE_OK	0
#else
#define DESCRIBE_OK	1
#endif

#ifdef NO_STOP
#define STOP	0
#else
#define STOP	1
#endif

#ifndef NO_COLOR
#define GRE "\e[32m"
#define RED	"\e[31m"
#define END	"\e[0m"
#else
#define GRE
#define RED
#define END
#endif

#define compare_noarg(format) \
	do { \
		_prepare(1); \
		i1 = printf(format); \
		_prepare(2); \
		i2 = ft_printf(format); \
		if (i1 != i2 || !equals()) { \
			msg("\n---\n"); \
			msg(RED "KO:" END " (\"" format "\")\n"); \
			msg2("expected: (retval = %d)\n", i1); \
			_print(1); \
			msg("\n"); \
			msg2("got: (retval = %d)\n", i2); \
			_print(2); \
			msg("\n---\n"); \
			if (stop) finish(EXIT_FAILURE); \
		} else if (show_ok) { \
			msg(GRE "OK:" END " (\"" format "\") "); \
			if (describe_ok) { \
				msg2("\ngot: (retval = %d)\n", i1); \
				_print(1); \
				msg("\n"); \
			} \
		} \
	} while (0)


#define compare(format, ...) \
	do { \
		_prepare(1); \
		i1 = printf(format, __VA_ARGS__); \
		_prepare(2); \
		i2 = ft_printf(format, __VA_ARGS__); \
		if (i1 != i2 || !equals()) { \
			msg("\n---\n"); \
			msg(RED "KO:" END " (\"" format "\", " #__VA_ARGS__ ")\n"); \
			msg2("expected: (retval = %d)\n", i1); \
			_print(1); \
			msg("\n"); \
			msg2("got: (retval = %d)\n", i2); \
			_print(2); \
			msg("\n---\n"); \
			if (stop) finish(EXIT_FAILURE); \
		} else if (show_ok) { \
			msg(GRE "OK:" END " (\"" format "\", " #__VA_ARGS__ ") "); \
			if (describe_ok) { \
				msg2("\ngot: (retval = %d)\n", i1); \
				_print(1); \
				msg("\n"); \
			} \
		} \
	} while (0)



static int	finish(int status)
{
	remove("_test1");
	remove("_test2");
	exit(status);
	return (0);
}

static inline void	msg(const char *msg)
{
	fputs(msg, stderr);
}

static void	msg2(const char *fmt, ...)
{
	va_list	ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
}

static inline void	init(void)
{
	if (setvbuf(stdout, NULL, _IOLBF, 0) != 0)
	{
		msg("unable to reset buffering");
		finish(EXIT_FAILURE);
	}
}

static void	_prepare(int i)
{
	if (!freopen(i == 1 ? "_test1" : "_test2", "w", stdout))
	{
		msg2("unable to open '_test%d' for writing", i);
		finish(EXIT_FAILURE);
	}
}

static void	_print(int i)
{
	FILE	*f;
	char	c;
	size_t	sz;

	f = fopen(i == 1 ? "_test1" : "_test2", "r");
	if (!f)
	{
		msg2("unable to open '_test%d' for reading", i);
		finish(EXIT_FAILURE);
	}
	msg("\"");
	do {
		sz = fread(&c, sizeof(char), 1, f);
		if (sz)
			fputc(c, stderr);
		else if (ferror(f))
		{
			msg("unexpected error...");
			finish(EXIT_FAILURE);
		}
	}
	while (sz);
	msg("\"");
	fclose(f);
}

static int	equals(void)
{
	int		ret;
	FILE	*f1, *f2;
	char	c1, c2;
	size_t	sz1, sz2;

	ret = 1;

	f1 = fopen("_test1", "r");
	if (!f1)
	{
		msg("unable to open '_test1' for reading");
		finish(EXIT_FAILURE);
	}
	f2 = fopen("_test2", "r");
	if (!f2)
	{
		msg("unable to open '_test2' for reading");
		finish(EXIT_FAILURE);
	}

	do {
		sz1 = fread(&c1, sizeof(char), 1, f1);
		sz2 = fread(&c2, sizeof(char), 1, f2);
		if (sz1 == 0 && sz2 == 0)
		{
			if (feof(f1) && feof(f2))
				goto finish;
			else
			{
				if (ferror(f1) || ferror(f2))
				{
					msg("unexpected error occurred...");
					finish(EXIT_FAILURE);
				}
				ret = 0;
				goto finish;
			}
		}
		if (sz1 != 1 || sz2 != 1 || c1 != c2)
		{
			ret = 0;
			goto finish;
		}
	}
	while (1);

finish:
	fclose(f1);
	fclose(f2);
	return (ret);
}

int	main(void)
{
	int			i1, i2;
	const int	show_ok = SHOW_OK;
	const int	describe_ok = DESCRIBE_OK;
	const int	stop = STOP;

	init();

	// double
	/* compare(" %f ", 0.12345); */
	/* compare(" %f ", 0.123456); */
	/* compare(" %f ", 0.1234567); */
	/* compare(" %f ", 0.12345678); */
	/* compare(" %f ", 0.1234567890); */
	/* compare(" %f ", 0.12345678901); */

	compare(" %.f ", 0.12345);
	compare(" %.2f ", 0.123456);
	compare(" %.7f ", 0.1234567);
	compare(" %.8f ", 0.12345678);
	compare(" %.9f ", 0.1234567890);
	compare(" %.10f ", 0.12345678901);

	compare(" %f ", 0.0);
	compare(" %f ", 2.0);
	compare(" %f ", -2.0);
	compare(" %f ", 12.0);
	compare(" %f ", -12.0);

	compare(" %f ", 0.3);
	compare(" %f ", 2.3);
	compare(" %f ", -2.3);
	compare(" %f ", 12.3);
	compare(" %f ", -12.3);

	compare(" %f ", 0.5);
	compare(" %f ", 2.5);
	compare(" %f ", -2.5);
	compare(" %f ", 12.5);
	compare(" %f ", -12.5);

	compare(" %f ", 0.99);
	compare(" %f ", 2.99);
	compare(" %f ", -2.99);
	compare(" %f ", 12.99);
	compare(" %f ", -12.99);

	compare(" %e ", 0.0);
	compare(" %e ", 2.0);
	compare(" %e ", -2.0);
	compare(" %e ", 12.0);
	compare(" %e ", -12.0);

	compare(" %e ", 0.3);
	compare(" %e ", 2.3);
	compare(" %e ", -2.3);
	compare(" %e ", 12.3);
	compare(" %e ", -12.3);

	compare(" %e ", 0.5);
	compare(" %e ", 2.5);
	compare(" %e ", -2.5);
	compare(" %e ", 12.5);
	compare(" %e ", -12.5);

	compare(" %e ", 0.99);
	compare(" %e ", 2.99);
	compare(" %e ", -2.99);
	compare(" %e ", 12.99);
	compare(" %e ", -12.99);


#if 1
	// simple strings
	compare_noarg("");
	compare_noarg(" ");
	compare_noarg("Hello World");
	compare_noarg(" ... ");

	// percents, simple
	compare_noarg(" %% ");
	compare_noarg(" %% %% ");

	// c, simple
	compare(" %c ", (char) 0);
	compare(" %c ", (char) 42);
	compare(" %c ", (char) -42);
	compare(" %c ", (char) 1024);
	compare(" %c ", (char) -1024);
	compare(" %c ", CHAR_MIN);
	compare(" %c ", CHAR_MAX);

	// d, simple
	compare(" %d ", 0);
	compare(" %d ", 42);
	compare(" %d ", -42);
	compare(" %d ", 1024);
	compare(" %d ", -1024);
	compare(" %d ", INT_MIN);
	compare(" %d ", INT_MAX);

	// i, simple
	compare(" %i ", 0);
	compare(" %i ", 42);
	compare(" %i ", -42);
	compare(" %i ", 1024);
	compare(" %i ", -1024);
	compare(" %i ", INT_MIN);
	compare(" %i ", INT_MAX);

	// u, simple
	compare(" %u ", 0);
	compare(" %u ", 42);
	compare(" %u ", -42);
	compare(" %u ", 1024);
	compare(" %u ", -1024);
	compare(" %u ", INT_MIN);
	compare(" %u ", INT_MAX);

#ifndef NO_OCTAL
	// o, simple
	compare(" %o ", 0);
	compare(" %o ", 42);
	compare(" %o ", -42);
	compare(" %o ", 1024);
	compare(" %o ", -1024);
	compare(" %o ", INT_MIN);
	compare(" %o ", INT_MAX);
#endif // ndef NO_OCTAL

	// x, simple
	compare(" %x ", 0);
	compare(" %x ", 42);
	compare(" %x ", -42);
	compare(" %x ", 1024);
	compare(" %x ", -1024);
	compare(" %x ", INT_MIN);
	compare(" %x ", INT_MAX);

	// X, simple
	compare(" %X ", 0);
	compare(" %X ", 42);
	compare(" %X ", -42);
	compare(" %X ", 1024);
	compare(" %X ", -1024);
	compare(" %X ", INT_MIN);
	compare(" %X ", INT_MAX);

	// p, simple
	compare(" %p ", (void*) 0);
	compare(" %p ", (void*) 42);
	compare(" %p ", (void*) -42);
	compare(" %p ", (void*) 0xdeadbeef);


#ifndef NO_BONUS

	// flag #
#ifndef NO_UNDEFINED_BEHAVIOR
	compare(" %#c ", 0);
	compare(" %#c ", 42);
	compare(" %#c ", -42);

	compare(" %#d ", 0);
	compare(" %#d ", 42);
	compare(" %#d ", -42);

	compare(" %#i ", 0);
	compare(" %#i ", 42);
	compare(" %#i ", -42);

	compare(" %#u ", 0);
	compare(" %#u ", 42);
	compare(" %#u ", -42);
#endif // ndef NO_UNDEFINED_BEHAVIOR

	compare(" %#x ", 0);
	compare(" %#x ", 42);
	compare(" %#x ", -42);

	compare(" %#X ", 0);
	compare(" %#X ", 42);
	compare(" %#X ", -42);

	// flag -
	compare(" %-c ", 0);
	compare(" %-c ", 42);
	compare(" %-c ", -42);
	compare(" %-1c ", 0);
	compare(" %-1c ", 42);
	compare(" %-1c ", -42);
	compare(" %-3c ", 0);
	compare(" %-3c ", 42);
	compare(" %-3c ", -42);
	compare(" %-5c ", 0);
	compare(" %-5c ", 42);
	compare(" %-5c ", -42);
	compare(" %-10c ", 0);
	compare(" %-10c ", 42);
	compare(" %-10c ", -42);

	compare(" %-d ", 0);
	compare(" %-d ", 42);
	compare(" %-d ", -42);
	compare(" %-1d ", 0);
	compare(" %-1d ", 42);
	compare(" %-1d ", -42);
	compare(" %-3d ", 0);
	compare(" %-3d ", 42);
	compare(" %-3d ", -42);
	compare(" %-5d ", 0);
	compare(" %-5d ", 42);
	compare(" %-5d ", -42);
	compare(" %-10d ", 0);
	compare(" %-10d ", 42);
	compare(" %-10d ", -42);

	compare(" %-i ", 0);
	compare(" %-i ", 42);
	compare(" %-i ", -42);
	compare(" %-1i ", 0);
	compare(" %-1i ", 42);
	compare(" %-1i ", -42);
	compare(" %-3i ", 0);
	compare(" %-3i ", 42);
	compare(" %-3i ", -42);
	compare(" %-5i ", 0);
	compare(" %-5i ", 42);
	compare(" %-5i ", -42);
	compare(" %-10i ", 0);
	compare(" %-10i ", 42);
	compare(" %-10i ", -42);

	compare(" %-u ", 0);
	compare(" %-u ", 42);
	compare(" %-u ", -42);
	compare(" %-1u ", 0);
	compare(" %-1u ", 42);
	compare(" %-1u ", -42);
	compare(" %-3u ", 0);
	compare(" %-3u ", 42);
	compare(" %-3u ", -42);
	compare(" %-5u ", 0);
	compare(" %-5u ", 42);
	compare(" %-5u ", -42);
	compare(" %-10u ", 0);
	compare(" %-10u ", 42);
	compare(" %-10u ", -42);

	compare(" %-x ", 0);
	compare(" %-x ", 42);
	compare(" %-x ", -42);
	compare(" %-1x ", 0);
	compare(" %-1x ", 42);
	compare(" %-1x ", -42);
	compare(" %-3x ", 0);
	compare(" %-3x ", 42);
	compare(" %-3x ", -42);
	compare(" %-5x ", 0);
	compare(" %-5x ", 42);
	compare(" %-5x ", -42);
	compare(" %-10x ", 0);
	compare(" %-10x ", 42);
	compare(" %-10x ", -42);

	compare(" %-X ", 0);
	compare(" %-X ", 42);
	compare(" %-X ", -42);
	compare(" %-1X ", 0);
	compare(" %-1X ", 42);
	compare(" %-1X ", -42);
	compare(" %-3X ", 0);
	compare(" %-3X ", 42);
	compare(" %-3X ", -42);
	compare(" %-5X ", 0);
	compare(" %-5X ", 42);
	compare(" %-5X ", -42);
	compare(" %-10X ", 0);
	compare(" %-10X ", 42);
	compare(" %-10X ", -42);

	// flag 0
#ifndef NO_UNDEFINED_BEHAVIOR
	compare(" %0c ", 0);
	compare(" %0c ", 42);
	compare(" %0c ", -42);
	compare(" %01c ", 0);
	compare(" %01c ", 42);
	compare(" %01c ", -42);
	compare(" %03c ", 0);
	compare(" %03c ", 42);
	compare(" %03c ", -42);
	compare(" %05c ", 0);
	compare(" %05c ", 42);
	compare(" %05c ", -42);
	compare(" %010c ", 0);
	compare(" %010c ", 42);
	compare(" %010c ", -42);
	compare(" %016c ", 0);
	compare(" %016c ", 42);
	compare(" %016c ", -42);
#endif // ndef NO_UNDEFINED_BEHAVIOR

	compare(" %0d ", 0);
	compare(" %0d ", 42);
	compare(" %0d ", -42);
	compare(" %01d ", 0);
	compare(" %01d ", 42);
	compare(" %01d ", -42);
	compare(" %03d ", 0);
	compare(" %03d ", 42);
	compare(" %03d ", -42);
	compare(" %05d ", 0);
	compare(" %05d ", 42);
	compare(" %05d ", -42);
	compare(" %010d ", 0);
	compare(" %010d ", 42);
	compare(" %010d ", -42);
	compare(" %016d ", 0);
	compare(" %016d ", 42);
	compare(" %016d ", -42);

	compare(" %0i ", 0);
	compare(" %0i ", 42);
	compare(" %0i ", -42);
	compare(" %01i ", 0);
	compare(" %01i ", 42);
	compare(" %01i ", -42);
	compare(" %03i ", 0);
	compare(" %03i ", 42);
	compare(" %03i ", -42);
	compare(" %05i ", 0);
	compare(" %05i ", 42);
	compare(" %05i ", -42);
	compare(" %010i ", 0);
	compare(" %010i ", 42);
	compare(" %010i ", -42);
	compare(" %016i ", 0);
	compare(" %016i ", 42);
	compare(" %016i ", -42);

	compare(" %0u ", 0);
	compare(" %0u ", 42);
	compare(" %0u ", -42);
	compare(" %01u ", 0);
	compare(" %01u ", 42);
	compare(" %01u ", -42);
	compare(" %03u ", 0);
	compare(" %03u ", 42);
	compare(" %03u ", -42);
	compare(" %05u ", 0);
	compare(" %05u ", 42);
	compare(" %05u ", -42);
	compare(" %010u ", 0);
	compare(" %010u ", 42);
	compare(" %010u ", -42);
	compare(" %016u ", 0);
	compare(" %016u ", 42);
	compare(" %016u ", -42);

	compare(" %0x ", 0);
	compare(" %0x ", 42);
	compare(" %0x ", -42);
	compare(" %01x ", 0);
	compare(" %01x ", 42);
	compare(" %01x ", -42);
	compare(" %03x ", 0);
	compare(" %03x ", 42);
	compare(" %03x ", -42);
	compare(" %05x ", 0);
	compare(" %05x ", 42);
	compare(" %05x ", -42);
	compare(" %010x ", 0);
	compare(" %010x ", 42);
	compare(" %010x ", -42);
	compare(" %016x ", 0);
	compare(" %016x ", 42);
	compare(" %016x ", -42);

	compare(" %0X ", 0);
	compare(" %0X ", 42);
	compare(" %0X ", -42);
	compare(" %01X ", 0);
	compare(" %01X ", 42);
	compare(" %01X ", -42);
	compare(" %03X ", 0);
	compare(" %03X ", 42);
	compare(" %03X ", -42);
	compare(" %05X ", 0);
	compare(" %05X ", 42);
	compare(" %05X ", -42);
	compare(" %010X ", 0);
	compare(" %010X ", 42);
	compare(" %010X ", -42);
	compare(" %016X ", 0);
	compare(" %016X ", 42);
	compare(" %016X ", -42);

	// flag space
	compare(" % d ", 0);
	compare(" % d ", 42);
	compare(" % d ", -42);

	compare(" % i ", 0);
	compare(" % i ", 42);
	compare(" % i ", -42);

#ifndef NO_UNDEFINED_BEHAVIOR
	compare(" % c ", 0);
	compare(" % c ", 42);
	compare(" % c ", -42);

	compare(" % u ", 0);
	compare(" % u ", 42);
	compare(" % u ", -42);

	compare(" % x ", 0);
	compare(" % x ", 42);
	compare(" % x ", -42);

	compare(" % X ", 0);
	compare(" % X ", 42);
	compare(" % X ", -42);
#endif // ndef NO_UNDEFINED_BEHAVIOR

	// flag +
	compare(" %+d ", 0);
	compare(" %+d ", 42);
	compare(" %+d ", -42);

	compare(" %+i ", 0);
	compare(" %+i ", 42);
	compare(" %+i ", -42);

#ifndef NO_UNDEFINED_BEHAVIOR
	compare(" %+c ", 0);
	compare(" %+c ", 42);
	compare(" %+c ", -42);

	compare(" %+u ", 0);
	compare(" %+u ", 42);
	compare(" %+u ", -42);

	compare(" %+x ", 0);
	compare(" %+x ", 42);
	compare(" %+x ", -42);

	compare(" %+X ", 0);
	compare(" %+X ", 42);
	compare(" %+X ", -42);
#endif // ndef NO_UNDEFINED_BEHAVIOR

	// field width
	compare(" %1c ", 0);
	compare(" %1c ", 42);
	compare(" %1c ", -42);
	compare(" %3c ", 0);
	compare(" %3c ", 42);
	compare(" %3c ", -42);
	compare(" %5c ", 0);
	compare(" %5c ", 42);
	compare(" %5c ", -42);

	compare(" %1d ", 0);
	compare(" %1d ", 42);
	compare(" %1d ", -42);
	compare(" %3d ", 0);
	compare(" %3d ", 42);
	compare(" %3d ", -42);
	compare(" %5d ", 0);
	compare(" %5d ", 42);
	compare(" %5d ", -42);

	compare(" %1i ", 0);
	compare(" %1i ", 42);
	compare(" %1i ", -42);
	compare(" %3i ", 0);
	compare(" %3i ", 42);
	compare(" %3i ", -42);
	compare(" %5i ", 0);
	compare(" %5i ", 42);
	compare(" %5i ", -42);

	compare(" %1u ", 0);
	compare(" %1u ", 42);
	compare(" %1u ", -42);
	compare(" %3u ", 0);
	compare(" %3u ", 42);
	compare(" %3u ", -42);
	compare(" %5u ", 0);
	compare(" %5u ", 42);
	compare(" %5u ", -42);

#ifndef NO_OCTAL
	compare(" %1o ", 0);
	compare(" %1o ", 42);
	compare(" %1o ", -42);
	compare(" %3o ", 0);
	compare(" %3o ", 42);
	compare(" %3o ", -42);
	compare(" %5o ", 0);
	compare(" %5o ", 42);
	compare(" %5o ", -42);
	compare(" %16o ", 0);
	compare(" %16o ", 42);
	compare(" %16o ", -42);
#endif // ndef NO_OCTAL

	compare(" %1x ", 0);
	compare(" %1x ", 42);
	compare(" %1x ", -42);
	compare(" %3x ", 0);
	compare(" %3x ", 42);
	compare(" %3x ", -42);
	compare(" %5x ", 0);
	compare(" %5x ", 42);
	compare(" %5x ", -42);
	compare(" %16x ", 0);
	compare(" %16x ", 42);
	compare(" %16x ", -42);

	compare(" %1X ", 0);
	compare(" %1X ", 42);
	compare(" %1X ", -42);
	compare(" %3X ", 0);
	compare(" %3X ", 42);
	compare(" %3X ", -42);
	compare(" %5X ", 0);
	compare(" %5X ", 42);
	compare(" %5X ", -42);
	compare(" %16X ", 0);
	compare(" %16X ", 42);
	compare(" %16X ", -42);

	// precision
#ifndef NO_UNDEFINED_BEHAVIOR
	compare(" %.c ", 0);
	compare(" %.c ", 42);
	compare(" %.c ", -42);
	compare(" %.1c ", 0);
	compare(" %.1c ", 42);
	compare(" %.1c ", -42);
	compare(" %.3c ", 0);
	compare(" %.3c ", 42);
	compare(" %.3c ", -42);
	compare(" %.5c ", 0);
	compare(" %.5c ", 42);
	compare(" %.5c ", -42);
#endif // ndef NO_UNDEFINED_BEHAVIOR

	compare(" %.d ", 0);
	compare(" %.d ", 42);
	compare(" %.d ", -42);
	compare(" %.1d ", 0);
	compare(" %.1d ", 42);
	compare(" %.1d ", -42);
	compare(" %.3d ", 0);
	compare(" %.3d ", 42);
	compare(" %.3d ", -42);
	compare(" %.5d ", 0);
	compare(" %.5d ", 42);
	compare(" %.5d ", -42);

	compare(" %.i ", 0);
	compare(" %.i ", 42);
	compare(" %.i ", -42);
	compare(" %.1i ", 0);
	compare(" %.1i ", 42);
	compare(" %.1i ", -42);
	compare(" %.3i ", 0);
	compare(" %.3i ", 42);
	compare(" %.3i ", -42);
	compare(" %.5i ", 0);
	compare(" %.5i ", 42);
	compare(" %.5i ", -42);

	compare(" %.u ", 0);
	compare(" %.u ", 42);
	compare(" %.u ", -42);
	compare(" %.1u ", 0);
	compare(" %.1u ", 42);
	compare(" %.1u ", -42);
	compare(" %.3u ", 0);
	compare(" %.3u ", 42);
	compare(" %.3u ", -42);
	compare(" %.5u ", 0);
	compare(" %.5u ", 42);
	compare(" %.5u ", -42);

#ifndef NO_OCTAL
	compare(" %.o ", 0);
	compare(" %.o ", 42);
	compare(" %.o ", -42);
	compare(" %.1o ", 0);
	compare(" %.1o ", 42);
	compare(" %.1o ", -42);
	compare(" %.5o ", 0);
	compare(" %.5o ", 42);
	compare(" %.5o ", -42);
	compare(" %.16o ", 0);
	compare(" %.16o ", 42);
	compare(" %.16o ", -42);
#endif // ndef NO_OCTAL

	compare(" %.x ", 0);
	compare(" %.x ", 42);
	compare(" %.x ", -42);
	compare(" %.1x ", 0);
	compare(" %.1x ", 42);
	compare(" %.1x ", -42);
	compare(" %.5x ", 0);
	compare(" %.5x ", 42);
	compare(" %.5x ", -42);
	compare(" %.16x ", 0);
	compare(" %.16x ", 42);
	compare(" %.16x ", -42);

	compare(" %.X ", 0);
	compare(" %.X ", 42);
	compare(" %.X ", -42);
	compare(" %.1X ", 0);
	compare(" %.1X ", 42);
	compare(" %.1X ", -42);
	compare(" %.3X ", 0);
	compare(" %.3X ", 42);
	compare(" %.3X ", -42);
	compare(" %.5X ", 0);
	compare(" %.5X ", 42);
	compare(" %.5X ", -42);

#ifndef NO_LENGTH
	// d, with length
	compare(" %hhd ", (char) 0);
	compare(" %hhd ", (char) 42);
	compare(" %hhd ", (char) -42);
	compare(" %hhd ", (char) 1024);
	compare(" %hhd ", (char) -1024);
	compare(" %hhd ", (char) CHAR_MIN);
	compare(" %hhd ", (char) CHAR_MAX);

	compare(" %hd ", (short) 0);
	compare(" %hd ", (short) 42);
	compare(" %hd ", (short) -42);
	compare(" %hd ", (short) 1024);
	compare(" %hd ", (short) -1024);
	compare(" %hd ", (short) SHRT_MIN);
	compare(" %hd ", (short) SHRT_MAX);

	compare(" %ld ", 0L);
	compare(" %ld ", 42L);
	compare(" %ld ", -42L);
	compare(" %ld ", 1024L);
	compare(" %ld ", -1024L);
	compare(" %ld ", LONG_MIN);
	compare(" %ld ", LONG_MAX);

	compare(" %lld ", 0LL);
	compare(" %lld ", 42LL);
	compare(" %lld ", -42LL);
	compare(" %lld ", 1024LL);
	compare(" %lld ", -1024LL);
	compare(" %lld ", LLONG_MIN);
	compare(" %lld ", LLONG_MAX);

	compare(" %zd ", (size_t) 0);
	compare(" %zd ", (size_t) 42);
	compare(" %zd ", (size_t) -42);
	compare(" %zd ", (size_t) 1024);
	compare(" %zd ", (size_t) -1024);
	compare(" %zd ", (size_t) LONG_MIN);
	compare(" %zd ", (size_t) LONG_MAX);

	// i, with length
	compare(" %hhi ", (char) 0);
	compare(" %hhi ", (char) 42);
	compare(" %hhi ", (char) -42);
	compare(" %hhi ", (char) 1024);
	compare(" %hhi ", (char) -1024);
	compare(" %hhi ", (char) CHAR_MIN);
	compare(" %hhi ", (char) CHAR_MAX);

	compare(" %hi ", (short) 0);
	compare(" %hi ", (short) 42);
	compare(" %hi ", (short) -42);
	compare(" %hi ", (short) 1024);
	compare(" %hi ", (short) -1024);
	compare(" %hi ", (short) SHRT_MIN);
	compare(" %hi ", (short) SHRT_MAX);

	compare(" %li ", 0L);
	compare(" %li ", 42L);
	compare(" %li ", -42L);
	compare(" %li ", 1024L);
	compare(" %li ", -1024L);
	compare(" %li ", LONG_MIN);
	compare(" %li ", LONG_MAX);

	compare(" %lli ", 0LL);
	compare(" %lli ", 42LL);
	compare(" %lli ", -42LL);
	compare(" %lli ", 1024LL);
	compare(" %lli ", -1024LL);
	compare(" %lli ", LLONG_MIN);
	compare(" %lli ", LLONG_MAX);

	compare(" %zi ", (size_t) 0);
	compare(" %zi ", (size_t) 42);
	compare(" %zi ", (size_t) -42);
	compare(" %zi ", (size_t) 1024);
	compare(" %zi ", (size_t) -1024);
	compare(" %zi ", (size_t) LONG_MIN);
	compare(" %zi ", (size_t) LONG_MAX);

	// u, with length
	compare(" %hhu ", (char) 0);
	compare(" %hhu ", (char) 42);
	compare(" %hhu ", (char) -42);
	compare(" %hhu ", (char) 1024);
	compare(" %hhu ", (char) -1024);
	compare(" %hhu ", (char) CHAR_MIN);
	compare(" %hhu ", (char) CHAR_MAX);

	compare(" %hu ", (short) 0);
	compare(" %hu ", (short) 42);
	compare(" %hu ", (short) -42);
	compare(" %hu ", (short) 1024);
	compare(" %hu ", (short) -1024);
	compare(" %hu ", (short) SHRT_MIN);
	compare(" %hu ", (short) SHRT_MAX);

	compare(" %lu ", 0L);
	compare(" %lu ", 42L);
	compare(" %lu ", -42L);
	compare(" %lu ", 1024L);
	compare(" %lu ", -1024L);
	compare(" %lu ", LONG_MIN);
	compare(" %lu ", LONG_MAX);

	compare(" %llu ", 0LL);
	compare(" %llu ", 42LL);
	compare(" %llu ", -42LL);
	compare(" %llu ", 1024LL);
	compare(" %llu ", -1024LL);
	compare(" %llu ", LLONG_MIN);
	compare(" %llu ", LLONG_MAX);

	compare(" %zu ", (size_t) 0);
	compare(" %zu ", (size_t) 42);
	compare(" %zu ", (size_t) -42);
	compare(" %zu ", (size_t) 1024);
	compare(" %zu ", (size_t) -1024);
	compare(" %zu ", (size_t) LONG_MIN);
	compare(" %zu ", (size_t) LONG_MAX);

	// o, with length
#ifndef NO_OCTAL
	compare(" %hho ", (char) 0);
	compare(" %hho ", (char) 42);
	compare(" %hho ", (char) -42);
	compare(" %hho ", (char) 1024);
	compare(" %hho ", (char) -1024);
	compare(" %hho ", (char) CHAR_MIN);
	compare(" %hho ", (char) CHAR_MAX);

	compare(" %ho ", (short) 0);
	compare(" %ho ", (short) 42);
	compare(" %ho ", (short) -42);
	compare(" %ho ", (short) 1024);
	compare(" %ho ", (short) -1024);
	compare(" %ho ", (short) SHRT_MIN);
	compare(" %ho ", (short) SHRT_MAX);

	compare(" %lo ", 0L);
	compare(" %lo ", 42L);
	compare(" %lo ", -42L);
	compare(" %lo ", 1024L);
	compare(" %lo ", -1024L);
	compare(" %lo ", LONG_MIN);
	compare(" %lo ", LONG_MAX);

	compare(" %llo ", 0LL);
	compare(" %llo ", 42LL);
	compare(" %llo ", -42LL);
	compare(" %llo ", 1024LL);
	compare(" %llo ", -1024LL);
	compare(" %llo ", LLONG_MIN);
	compare(" %llo ", LLONG_MAX);

	compare(" %zo ", (size_t) 0);
	compare(" %zo ", (size_t) 42);
	compare(" %zo ", (size_t) -42);
	compare(" %zo ", (size_t) 1024);
	compare(" %zo ", (size_t) -1024);
	compare(" %zo ", (size_t) LONG_MIN);
	compare(" %zo ", (size_t) LONG_MAX);
#endif // ndef NO_OCTAL

	// x, with length
	compare(" %hhx ", (char) 0);
	compare(" %hhx ", (char) 42);
	compare(" %hhx ", (char) -42);
	compare(" %hhx ", (char) 1024);
	compare(" %hhx ", (char) -1024);
	compare(" %hhx ", (char) CHAR_MIN);
	compare(" %hhx ", (char) CHAR_MAX);

	compare(" %hx ", (short) 0);
	compare(" %hx ", (short) 42);
	compare(" %hx ", (short) -42);
	compare(" %hx ", (short) 1024);
	compare(" %hx ", (short) -1024);
	compare(" %hx ", (short) SHRT_MIN);
	compare(" %hx ", (short) SHRT_MAX);

	compare(" %lx ", 0L);
	compare(" %lx ", 42L);
	compare(" %lx ", -42L);
	compare(" %lx ", 1024L);
	compare(" %lx ", -1024L);
	compare(" %lx ", LONG_MIN);
	compare(" %lx ", LONG_MAX);

	compare(" %llx ", 0LL);
	compare(" %llx ", 42LL);
	compare(" %llx ", -42LL);
	compare(" %llx ", 1024LL);
	compare(" %llx ", -1024LL);
	compare(" %llx ", LLONG_MIN);
	compare(" %llx ", LLONG_MAX);

	compare(" %zx ", (size_t) 0);
	compare(" %zx ", (size_t) 42);
	compare(" %zx ", (size_t) -42);
	compare(" %zx ", (size_t) 1024);
	compare(" %zx ", (size_t) -1024);
	compare(" %zx ", (size_t) LONG_MIN);
	compare(" %zx ", (size_t) LONG_MAX);

	// X, with length
	compare(" %hhX ", (char) 0);
	compare(" %hhX ", (char) 42);
	compare(" %hhX ", (char) -42);
	compare(" %hhX ", (char) 1024);
	compare(" %hhX ", (char) -1024);
	compare(" %hhX ", (char) CHAR_MIN);
	compare(" %hhX ", (char) CHAR_MAX);

	compare(" %hX ", (short) 0);
	compare(" %hX ", (short) 42);
	compare(" %hX ", (short) -42);
	compare(" %hX ", (short) 1024);
	compare(" %hX ", (short) -1024);
	compare(" %hX ", (short) SHRT_MIN);
	compare(" %hX ", (short) SHRT_MAX);

	compare(" %lX ", 0L);
	compare(" %lX ", 42L);
	compare(" %lX ", -42L);
	compare(" %lX ", 1024L);
	compare(" %lX ", -1024L);
	compare(" %lX ", LONG_MIN);
	compare(" %lX ", LONG_MAX);

	compare(" %llX ", 0LL);
	compare(" %llX ", 42LL);
	compare(" %llX ", -42LL);
	compare(" %llX ", 1024LL);
	compare(" %llX ", -1024LL);
	compare(" %llX ", LLONG_MIN);
	compare(" %llX ", LLONG_MAX);

	compare(" %zX ", (size_t) 0);
	compare(" %zX ", (size_t) 42);
	compare(" %zX ", (size_t) -42);
	compare(" %zX ", (size_t) 1024);
	compare(" %zX ", (size_t) -1024);
	compare(" %zX ", (size_t) LONG_MIN);
	compare(" %zX ", (size_t) LONG_MAX);

#endif // ndef NO_LENGTH

	// d, precision and field width
	compare(" %10.5d ", 0);
	compare(" %10.5d ", 42);
	compare(" %10.5d ", -42);

	// i, precision and field width
	compare(" %10.5i ", 0);
	compare(" %10.5i ", 42);
	compare(" %10.5i ", -42);

	// u, precision and field width
	compare(" %10.5u ", 0);
	compare(" %10.5u ", 42);
	compare(" %10.5u ", -42);
#ifndef NO_OCTAL
	// o, precision and field width
	compare(" %10.5o ", 0);
	compare(" %10.5o ", 42);
	compare(" %10.5o ", -42);
#endif // ndef NO_OCTAL
	// x, precision and field width
	compare(" %10.5x ", 0);
	compare(" %10.5x ", 42);
	compare(" %10.5x ", -42);

	// X, precision and field width
	compare(" %10.5X ", 0);
	compare(" %10.5X ", 42);
	compare(" %10.5X ", -42);

	// d, flag 0 and precision
	compare(" %048.23d ", 0);

	// d, flag space and flag - and precision
	compare(" % -6.2d ", 0);


	// Mix from other testers

	compare(" 42% 024d42 ", 0);
	compare("\\!/% 036.28d\\!/", 0);
	compare("\\!/% 063d\\!/", 0);
	compare("^.^/% 036d^.^/", -1679778188);
	compare("\\!/% 042.12d\\!/", -1759055112);
	compare("!%+-60.30d!", 0);
	compare("\\!/%+012.11d\\!/", 0);
	compare("\\!/%+022.2d\\!/", 0);
	compare("%+032d", 0);
	compare("42%0 20d42", 0);
	compare("42%0 60.50d42", 0);
	compare("\\!/%#48.46x\\!/", -2088619264);
	compare("!%#28.2x!", 0);
	compare("^.^/%#42.22x^.^/", -1060913622);
	compare("\\!/%0#16x\\!/", -1505893108);

	compare_noarg("%-5%");
	compare_noarg("%-05%");
	compare("%5.0i", 0);
	compare("%5.i", 0);
	compare("%-5.0i", 0);
	compare("%-5.i", 0);
	compare("%5.0d", 0);
	compare("%5.d", 0);
	compare("%-5.0d", 0);
	compare("%-5.d", 0);
	compare("%5.0u", 0);
	compare("%5.u", 0);
	compare("%-5.0u", 0);
	compare("%-5.u", 0);
	compare("%5.0x", 0);
	compare("%5.x", 0);
	compare("%-5.0x", 0);
	compare("%-5.x", 0);
	compare("%5.0x", 0);
	compare("%5.x", 0);
	compare("%-5.0x", 0);
	compare("%-5.x", 0);
	compare("%5.0X", 0);
	compare("%5.X", 0);
	compare("%-5.0X", 0);
	compare("%-5.X", 0);
	compare_noarg("percent 2 %12%");
	compare_noarg("percent 3 %-12%");
	compare("%20.d", 0);
	compare("%20.i", 0);
	compare("%20.u", 0u);
	compare("%20.x", 0u);
	compare("%20.X", 0u);

	compare("%1.d, %1.d, %1.d, %1.d, %1.d, %1.d, %1.d, %1.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%1.0d, %1.0d, %1.0d, %1.0d, %1.0d, %1.0d, %1.0d, %1.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%5.d, %5.d, %5.d, %5.d, %5.d, %5.d, %5.d, %5.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%5.0d, %5.0d, %5.0d, %5.0d, %5.0d, %5.0d, %5.0d, %5.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%10.d, %10.d, %10.d, %10.d, %10.d, %10.d, %10.d, %10.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%10.0d, %10.0d, %10.0d, %10.0d, %10.0d, %10.0d, %10.0d, %10.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%100.d, %100.d, %100.d, %100.d, %100.d, %100.d, %100.d, %100.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%100.0d, %100.0d, %100.0d, %100.0d, %100.0d, %100.0d, %100.0d, %100.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+.d, %+.d, %+.d, %+.d, %+.d, %+.d, %+.d, %+.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+.0d, %+.0d, %+.0d, %+.0d, %+.0d, %+.0d, %+.0d, %+.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+1.d, %+1.d, %+1.d, %+1.d, %+1.d, %+1.d, %+1.d, %+1.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+1.0d, %+1.0d, %+1.0d, %+1.0d, %+1.0d, %+1.0d, %+1.0d, %+1.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+5.d, %+5.d, %+5.d, %+5.d, %+5.d, %+5.d, %+5.d, %+5.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+5.0d, %+5.0d, %+5.0d, %+5.0d, %+5.0d, %+5.0d, %+5.0d, %+5.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+10.d, %+10.d, %+10.d, %+10.d, %+10.d, %+10.d, %+10.d, %+10.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+10.0d, %+10.0d, %+10.0d, %+10.0d, %+10.0d, %+10.0d, %+10.0d, %+10.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+100.d, %+100.d, %+100.d, %+100.d, %+100.d, %+100.d, %+100.d, %+100.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+100.0d, %+100.0d, %+100.0d, %+100.0d, %+100.0d, %+100.0d, %+100.0d, %+100.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+100.0d, %+100.0d, %+100.0d, %+100.0d, %+100.0d, %+100.0d, %+100.0d, %+100.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% .d, % .d, % .d, % .d, % .d, % .d, % .d, % .d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% .d, % .d, % .d, % .d, % .d, % .d, % .d, % .d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% .0d, % .0d, % .0d, % .0d, % .0d, % .0d, % .0d, % .0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% .0d, % .0d, % .0d, % .0d, % .0d, % .0d, % .0d, % .0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 1.d, % 1.d, % 1.d, % 1.d, % 1.d, % 1.d, % 1.d, % 1.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 1.d, % 1.d, % 1.d, % 1.d, % 1.d, % 1.d, % 1.d, % 1.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 1.0d, % 1.0d, % 1.0d, % 1.0d, % 1.0d, % 1.0d, % 1.0d, % 1.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 1.0d, % 1.0d, % 1.0d, % 1.0d, % 1.0d, % 1.0d, % 1.0d, % 1.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 5.d, % 5.d, % 5.d, % 5.d, % 5.d, % 5.d, % 5.d, % 5.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 5.d, % 5.d, % 5.d, % 5.d, % 5.d, % 5.d, % 5.d, % 5.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 5.0d, % 5.0d, % 5.0d, % 5.0d, % 5.0d, % 5.0d, % 5.0d, % 5.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 5.0d, % 5.0d, % 5.0d, % 5.0d, % 5.0d, % 5.0d, % 5.0d, % 5.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 10.d, % 10.d, % 10.d, % 10.d, % 10.d, % 10.d, % 10.d, % 10.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 10.d, % 10.d, % 10.d, % 10.d, % 10.d, % 10.d, % 10.d, % 10.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 10.0d, % 10.0d, % 10.0d, % 10.0d, % 10.0d, % 10.0d, % 10.0d, % 10.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 10.0d, % 10.0d, % 10.0d, % 10.0d, % 10.0d, % 10.0d, % 10.0d, % 10.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 100.d, % 100.d, % 100.d, % 100.d, % 100.d, % 100.d, % 100.d, % 100.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 100.d, % 100.d, % 100.d, % 100.d, % 100.d, % 100.d, % 100.d, % 100.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 100.0d, % 100.0d, % 100.0d, % 100.0d, % 100.0d, % 100.0d, % 100.0d, % 100.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 100.0d, % 100.0d, % 100.0d, % 100.0d, % 100.0d, % 100.0d, % 100.0d, % 100.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%01.d, %01.d, %01.d, %01.d, %01.d, %01.d, %01.d, %01.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%01.d, %01.d, %01.d, %01.d, %01.d, %01.d, %01.d, %01.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%01.0d, %01.0d, %01.0d, %01.0d, %01.0d, %01.0d, %01.0d, %01.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%01.0d, %01.0d, %01.0d, %01.0d, %01.0d, %01.0d, %01.0d, %01.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%05.d, %05.d, %05.d, %05.d, %05.d, %05.d, %05.d, %05.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%05.d, %05.d, %05.d, %05.d, %05.d, %05.d, %05.d, %05.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%05.0d, %05.0d, %05.0d, %05.0d, %05.0d, %05.0d, %05.0d, %05.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%05.0d, %05.0d, %05.0d, %05.0d, %05.0d, %05.0d, %05.0d, %05.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%010.d, %010.d, %010.d, %010.d, %010.d, %010.d, %010.d, %010.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%010.d, %010.d, %010.d, %010.d, %010.d, %010.d, %010.d, %010.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%010.0d, %010.0d, %010.0d, %010.0d, %010.0d, %010.0d, %010.0d, %010.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%010.0d, %010.0d, %010.0d, %010.0d, %010.0d, %010.0d, %010.0d, %010.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0100.d, %0100.d, %0100.d, %0100.d, %0100.d, %0100.d, %0100.d, %0100.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0100.d, %0100.d, %0100.d, %0100.d, %0100.d, %0100.d, %0100.d, %0100.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0100.0d, %0100.0d, %0100.0d, %0100.0d, %0100.0d, %0100.0d, %0100.0d, %0100.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0100.0d, %0100.0d, %0100.0d, %0100.0d, %0100.0d, %0100.0d, %0100.0d, %0100.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+.d, %0+.d, %0+.d, %0+.d, %0+.d, %0+.d, %0+.d, %0+.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+.d, %0+.d, %0+.d, %0+.d, %0+.d, %0+.d, %0+.d, %0+.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+.0d, %0+.0d, %0+.0d, %0+.0d, %0+.0d, %0+.0d, %0+.0d, %0+.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+.0d, %0+.0d, %0+.0d, %0+.0d, %0+.0d, %0+.0d, %0+.0d, %0+.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+1.d, %0+1.d, %0+1.d, %0+1.d, %0+1.d, %0+1.d, %0+1.d, %0+1.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+1.d, %0+1.d, %0+1.d, %0+1.d, %0+1.d, %0+1.d, %0+1.d, %0+1.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+1.0d, %0+1.0d, %0+1.0d, %0+1.0d, %0+1.0d, %0+1.0d, %0+1.0d, %0+1.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+1.0d, %0+1.0d, %0+1.0d, %0+1.0d, %0+1.0d, %0+1.0d, %0+1.0d, %0+1.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+5.d, %0+5.d, %0+5.d, %0+5.d, %0+5.d, %0+5.d, %0+5.d, %0+5.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+5.d, %0+5.d, %0+5.d, %0+5.d, %0+5.d, %0+5.d, %0+5.d, %0+5.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+5.0d, %0+5.0d, %0+5.0d, %0+5.0d, %0+5.0d, %0+5.0d, %0+5.0d, %0+5.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+5.0d, %0+5.0d, %0+5.0d, %0+5.0d, %0+5.0d, %0+5.0d, %0+5.0d, %0+5.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+10.d, %0+10.d, %0+10.d, %0+10.d, %0+10.d, %0+10.d, %0+10.d, %0+10.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+10.d, %0+10.d, %0+10.d, %0+10.d, %0+10.d, %0+10.d, %0+10.d, %0+10.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+10.0d, %0+10.0d, %0+10.0d, %0+10.0d, %0+10.0d, %0+10.0d, %0+10.0d, %0+10.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+10.0d, %0+10.0d, %0+10.0d, %0+10.0d, %0+10.0d, %0+10.0d, %0+10.0d, %0+10.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+100.d, %0+100.d, %0+100.d, %0+100.d, %0+100.d, %0+100.d, %0+100.d, %0+100.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+100.d, %0+100.d, %0+100.d, %0+100.d, %0+100.d, %0+100.d, %0+100.d, %0+100.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+100.0d, %0+100.0d, %0+100.0d, %0+100.0d, %0+100.0d, %0+100.0d, %0+100.0d, %0+100.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+100.0d, %0+100.0d, %0+100.0d, %0+100.0d, %0+100.0d, %0+100.0d, %0+100.0d, %0+100.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 .d, %0 .d, %0 .d, %0 .d, %0 .d, %0 .d, %0 .d, %0 .d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 .d, %0 .d, %0 .d, %0 .d, %0 .d, %0 .d, %0 .d, %0 .d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 .0d, %0 .0d, %0 .0d, %0 .0d, %0 .0d, %0 .0d, %0 .0d, %0 .0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 .0d, %0 .0d, %0 .0d, %0 .0d, %0 .0d, %0 .0d, %0 .0d, %0 .0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 1.d, %0 1.d, %0 1.d, %0 1.d, %0 1.d, %0 1.d, %0 1.d, %0 1.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 1.d, %0 1.d, %0 1.d, %0 1.d, %0 1.d, %0 1.d, %0 1.d, %0 1.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 1.0d, %0 1.0d, %0 1.0d, %0 1.0d, %0 1.0d, %0 1.0d, %0 1.0d, %0 1.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 1.0d, %0 1.0d, %0 1.0d, %0 1.0d, %0 1.0d, %0 1.0d, %0 1.0d, %0 1.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 5.d, %0 5.d, %0 5.d, %0 5.d, %0 5.d, %0 5.d, %0 5.d, %0 5.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 5.d, %0 5.d, %0 5.d, %0 5.d, %0 5.d, %0 5.d, %0 5.d, %0 5.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 5.0d, %0 5.0d, %0 5.0d, %0 5.0d, %0 5.0d, %0 5.0d, %0 5.0d, %0 5.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 5.0d, %0 5.0d, %0 5.0d, %0 5.0d, %0 5.0d, %0 5.0d, %0 5.0d, %0 5.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 10.d, %0 10.d, %0 10.d, %0 10.d, %0 10.d, %0 10.d, %0 10.d, %0 10.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 10.d, %0 10.d, %0 10.d, %0 10.d, %0 10.d, %0 10.d, %0 10.d, %0 10.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 10.0d, %0 10.0d, %0 10.0d, %0 10.0d, %0 10.0d, %0 10.0d, %0 10.0d, %0 10.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 10.0d, %0 10.0d, %0 10.0d, %0 10.0d, %0 10.0d, %0 10.0d, %0 10.0d, %0 10.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 100.d, %0 100.d, %0 100.d, %0 100.d, %0 100.d, %0 100.d, %0 100.d, %0 100.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 100.d, %0 100.d, %0 100.d, %0 100.d, %0 100.d, %0 100.d, %0 100.d, %0 100.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 100.0d, %0 100.0d, %0 100.0d, %0 100.0d, %0 100.0d, %0 100.0d, %0 100.0d, %0 100.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 100.0d, %0 100.0d, %0 100.0d, %0 100.0d, %0 100.0d, %0 100.0d, %0 100.0d, %0 100.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-1.d, %-1.d, %-1.d, %-1.d, %-1.d, %-1.d, %-1.d, %-1.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-1.d, %-1.d, %-1.d, %-1.d, %-1.d, %-1.d, %-1.d, %-1.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-1.0d, %-1.0d, %-1.0d, %-1.0d, %-1.0d, %-1.0d, %-1.0d, %-1.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-1.0d, %-1.0d, %-1.0d, %-1.0d, %-1.0d, %-1.0d, %-1.0d, %-1.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-5.d, %-5.d, %-5.d, %-5.d, %-5.d, %-5.d, %-5.d, %-5.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-5.d, %-5.d, %-5.d, %-5.d, %-5.d, %-5.d, %-5.d, %-5.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-5.0d, %-5.0d, %-5.0d, %-5.0d, %-5.0d, %-5.0d, %-5.0d, %-5.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-5.0d, %-5.0d, %-5.0d, %-5.0d, %-5.0d, %-5.0d, %-5.0d, %-5.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-10.d, %-10.d, %-10.d, %-10.d, %-10.d, %-10.d, %-10.d, %-10.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-10.d, %-10.d, %-10.d, %-10.d, %-10.d, %-10.d, %-10.d, %-10.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-10.0d, %-10.0d, %-10.0d, %-10.0d, %-10.0d, %-10.0d, %-10.0d, %-10.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-10.0d, %-10.0d, %-10.0d, %-10.0d, %-10.0d, %-10.0d, %-10.0d, %-10.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-100.d, %-100.d, %-100.d, %-100.d, %-100.d, %-100.d, %-100.d, %-100.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-100.d, %-100.d, %-100.d, %-100.d, %-100.d, %-100.d, %-100.d, %-100.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-100.0d, %-100.0d, %-100.0d, %-100.0d, %-100.0d, %-100.0d, %-100.0d, %-100.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-100.0d, %-100.0d, %-100.0d, %-100.0d, %-100.0d, %-100.0d, %-100.0d, %-100.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+.d, %-+.d, %-+.d, %-+.d, %-+.d, %-+.d, %-+.d, %-+.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+.d, %-+.d, %-+.d, %-+.d, %-+.d, %-+.d, %-+.d, %-+.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+.0d, %-+.0d, %-+.0d, %-+.0d, %-+.0d, %-+.0d, %-+.0d, %-+.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+.0d, %-+.0d, %-+.0d, %-+.0d, %-+.0d, %-+.0d, %-+.0d, %-+.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+1.d, %-+1.d, %-+1.d, %-+1.d, %-+1.d, %-+1.d, %-+1.d, %-+1.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+1.d, %-+1.d, %-+1.d, %-+1.d, %-+1.d, %-+1.d, %-+1.d, %-+1.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+1.0d, %-+1.0d, %-+1.0d, %-+1.0d, %-+1.0d, %-+1.0d, %-+1.0d, %-+1.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+1.0d, %-+1.0d, %-+1.0d, %-+1.0d, %-+1.0d, %-+1.0d, %-+1.0d, %-+1.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+5.d, %-+5.d, %-+5.d, %-+5.d, %-+5.d, %-+5.d, %-+5.d, %-+5.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+5.d, %-+5.d, %-+5.d, %-+5.d, %-+5.d, %-+5.d, %-+5.d, %-+5.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+5.0d, %-+5.0d, %-+5.0d, %-+5.0d, %-+5.0d, %-+5.0d, %-+5.0d, %-+5.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+5.0d, %-+5.0d, %-+5.0d, %-+5.0d, %-+5.0d, %-+5.0d, %-+5.0d, %-+5.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+10.d, %-+10.d, %-+10.d, %-+10.d, %-+10.d, %-+10.d, %-+10.d, %-+10.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+10.d, %-+10.d, %-+10.d, %-+10.d, %-+10.d, %-+10.d, %-+10.d, %-+10.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+10.0d, %-+10.0d, %-+10.0d, %-+10.0d, %-+10.0d, %-+10.0d, %-+10.0d, %-+10.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+10.0d, %-+10.0d, %-+10.0d, %-+10.0d, %-+10.0d, %-+10.0d, %-+10.0d, %-+10.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+100.d, %-+100.d, %-+100.d, %-+100.d, %-+100.d, %-+100.d, %-+100.d, %-+100.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+100.d, %-+100.d, %-+100.d, %-+100.d, %-+100.d, %-+100.d, %-+100.d, %-+100.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+100.0d, %-+100.0d, %-+100.0d, %-+100.0d, %-+100.0d, %-+100.0d, %-+100.0d, %-+100.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+100.0d, %-+100.0d, %-+100.0d, %-+100.0d, %-+100.0d, %-+100.0d, %-+100.0d, %-+100.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- .d, %- .d, %- .d, %- .d, %- .d, %- .d, %- .d, %- .d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- .d, %- .d, %- .d, %- .d, %- .d, %- .d, %- .d, %- .d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- .0d, %- .0d, %- .0d, %- .0d, %- .0d, %- .0d, %- .0d, %- .0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- .0d, %- .0d, %- .0d, %- .0d, %- .0d, %- .0d, %- .0d, %- .0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 1.d, %- 1.d, %- 1.d, %- 1.d, %- 1.d, %- 1.d, %- 1.d, %- 1.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 1.d, %- 1.d, %- 1.d, %- 1.d, %- 1.d, %- 1.d, %- 1.d, %- 1.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 1.0d, %- 1.0d, %- 1.0d, %- 1.0d, %- 1.0d, %- 1.0d, %- 1.0d, %- 1.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 1.0d, %- 1.0d, %- 1.0d, %- 1.0d, %- 1.0d, %- 1.0d, %- 1.0d, %- 1.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 5.d, %- 5.d, %- 5.d, %- 5.d, %- 5.d, %- 5.d, %- 5.d, %- 5.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 5.d, %- 5.d, %- 5.d, %- 5.d, %- 5.d, %- 5.d, %- 5.d, %- 5.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 5.0d, %- 5.0d, %- 5.0d, %- 5.0d, %- 5.0d, %- 5.0d, %- 5.0d, %- 5.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 5.0d, %- 5.0d, %- 5.0d, %- 5.0d, %- 5.0d, %- 5.0d, %- 5.0d, %- 5.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 10.d, %- 10.d, %- 10.d, %- 10.d, %- 10.d, %- 10.d, %- 10.d, %- 10.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 10.d, %- 10.d, %- 10.d, %- 10.d, %- 10.d, %- 10.d, %- 10.d, %- 10.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 10.0d, %- 10.0d, %- 10.0d, %- 10.0d, %- 10.0d, %- 10.0d, %- 10.0d, %- 10.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 10.0d, %- 10.0d, %- 10.0d, %- 10.0d, %- 10.0d, %- 10.0d, %- 10.0d, %- 10.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 100.d, %- 100.d, %- 100.d, %- 100.d, %- 100.d, %- 100.d, %- 100.d, %- 100.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 100.d, %- 100.d, %- 100.d, %- 100.d, %- 100.d, %- 100.d, %- 100.d, %- 100.d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 100.0d, %- 100.0d, %- 100.0d, %- 100.0d, %- 100.0d, %- 100.0d, %- 100.0d, %- 100.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 100.0d, %- 100.0d, %- 100.0d, %- 100.0d, %- 100.0d, %- 100.0d, %- 100.0d, %- 100.0d", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%1.i, %1.i, %1.i, %1.i, %1.i, %1.i, %1.i, %1.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%1.i, %1.i, %1.i, %1.i, %1.i, %1.i, %1.i, %1.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%1.0i, %1.0i, %1.0i, %1.0i, %1.0i, %1.0i, %1.0i, %1.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%1.0i, %1.0i, %1.0i, %1.0i, %1.0i, %1.0i, %1.0i, %1.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%5.i, %5.i, %5.i, %5.i, %5.i, %5.i, %5.i, %5.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%5.i, %5.i, %5.i, %5.i, %5.i, %5.i, %5.i, %5.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%5.0i, %5.0i, %5.0i, %5.0i, %5.0i, %5.0i, %5.0i, %5.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%5.0i, %5.0i, %5.0i, %5.0i, %5.0i, %5.0i, %5.0i, %5.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%10.i, %10.i, %10.i, %10.i, %10.i, %10.i, %10.i, %10.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%10.i, %10.i, %10.i, %10.i, %10.i, %10.i, %10.i, %10.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%10.0i, %10.0i, %10.0i, %10.0i, %10.0i, %10.0i, %10.0i, %10.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%10.0i, %10.0i, %10.0i, %10.0i, %10.0i, %10.0i, %10.0i, %10.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%100.i, %100.i, %100.i, %100.i, %100.i, %100.i, %100.i, %100.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%100.i, %100.i, %100.i, %100.i, %100.i, %100.i, %100.i, %100.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%100.0i, %100.0i, %100.0i, %100.0i, %100.0i, %100.0i, %100.0i, %100.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%100.0i, %100.0i, %100.0i, %100.0i, %100.0i, %100.0i, %100.0i, %100.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+.i, %+.i, %+.i, %+.i, %+.i, %+.i, %+.i, %+.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+.i, %+.i, %+.i, %+.i, %+.i, %+.i, %+.i, %+.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+.0i, %+.0i, %+.0i, %+.0i, %+.0i, %+.0i, %+.0i, %+.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+.0i, %+.0i, %+.0i, %+.0i, %+.0i, %+.0i, %+.0i, %+.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+1.i, %+1.i, %+1.i, %+1.i, %+1.i, %+1.i, %+1.i, %+1.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+1.i, %+1.i, %+1.i, %+1.i, %+1.i, %+1.i, %+1.i, %+1.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+1.0i, %+1.0i, %+1.0i, %+1.0i, %+1.0i, %+1.0i, %+1.0i, %+1.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+1.0i, %+1.0i, %+1.0i, %+1.0i, %+1.0i, %+1.0i, %+1.0i, %+1.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+5.i, %+5.i, %+5.i, %+5.i, %+5.i, %+5.i, %+5.i, %+5.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+5.i, %+5.i, %+5.i, %+5.i, %+5.i, %+5.i, %+5.i, %+5.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+5.0i, %+5.0i, %+5.0i, %+5.0i, %+5.0i, %+5.0i, %+5.0i, %+5.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+5.0i, %+5.0i, %+5.0i, %+5.0i, %+5.0i, %+5.0i, %+5.0i, %+5.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+10.i, %+10.i, %+10.i, %+10.i, %+10.i, %+10.i, %+10.i, %+10.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+10.i, %+10.i, %+10.i, %+10.i, %+10.i, %+10.i, %+10.i, %+10.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+10.0i, %+10.0i, %+10.0i, %+10.0i, %+10.0i, %+10.0i, %+10.0i, %+10.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+10.0i, %+10.0i, %+10.0i, %+10.0i, %+10.0i, %+10.0i, %+10.0i, %+10.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+100.i, %+100.i, %+100.i, %+100.i, %+100.i, %+100.i, %+100.i, %+100.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+100.i, %+100.i, %+100.i, %+100.i, %+100.i, %+100.i, %+100.i, %+100.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+100.0i, %+100.0i, %+100.0i, %+100.0i, %+100.0i, %+100.0i, %+100.0i, %+100.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%+100.0i, %+100.0i, %+100.0i, %+100.0i, %+100.0i, %+100.0i, %+100.0i, %+100.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% .i, % .i, % .i, % .i, % .i, % .i, % .i, % .i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% .i, % .i, % .i, % .i, % .i, % .i, % .i, % .i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% .0i, % .0i, % .0i, % .0i, % .0i, % .0i, % .0i, % .0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% .0i, % .0i, % .0i, % .0i, % .0i, % .0i, % .0i, % .0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 1.i, % 1.i, % 1.i, % 1.i, % 1.i, % 1.i, % 1.i, % 1.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 1.i, % 1.i, % 1.i, % 1.i, % 1.i, % 1.i, % 1.i, % 1.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 1.0i, % 1.0i, % 1.0i, % 1.0i, % 1.0i, % 1.0i, % 1.0i, % 1.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 1.0i, % 1.0i, % 1.0i, % 1.0i, % 1.0i, % 1.0i, % 1.0i, % 1.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 5.i, % 5.i, % 5.i, % 5.i, % 5.i, % 5.i, % 5.i, % 5.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 5.i, % 5.i, % 5.i, % 5.i, % 5.i, % 5.i, % 5.i, % 5.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 5.0i, % 5.0i, % 5.0i, % 5.0i, % 5.0i, % 5.0i, % 5.0i, % 5.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 5.0i, % 5.0i, % 5.0i, % 5.0i, % 5.0i, % 5.0i, % 5.0i, % 5.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 10.i, % 10.i, % 10.i, % 10.i, % 10.i, % 10.i, % 10.i, % 10.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 10.i, % 10.i, % 10.i, % 10.i, % 10.i, % 10.i, % 10.i, % 10.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 10.0i, % 10.0i, % 10.0i, % 10.0i, % 10.0i, % 10.0i, % 10.0i, % 10.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 10.0i, % 10.0i, % 10.0i, % 10.0i, % 10.0i, % 10.0i, % 10.0i, % 10.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 100.i, % 100.i, % 100.i, % 100.i, % 100.i, % 100.i, % 100.i, % 100.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 100.i, % 100.i, % 100.i, % 100.i, % 100.i, % 100.i, % 100.i, % 100.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 100.0i, % 100.0i, % 100.0i, % 100.0i, % 100.0i, % 100.0i, % 100.0i, % 100.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("% 100.0i, % 100.0i, % 100.0i, % 100.0i, % 100.0i, % 100.0i, % 100.0i, % 100.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%01.i, %01.i, %01.i, %01.i, %01.i, %01.i, %01.i, %01.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%01.i, %01.i, %01.i, %01.i, %01.i, %01.i, %01.i, %01.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%01.0i, %01.0i, %01.0i, %01.0i, %01.0i, %01.0i, %01.0i, %01.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%01.0i, %01.0i, %01.0i, %01.0i, %01.0i, %01.0i, %01.0i, %01.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%05.i, %05.i, %05.i, %05.i, %05.i, %05.i, %05.i, %05.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%05.i, %05.i, %05.i, %05.i, %05.i, %05.i, %05.i, %05.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%05.0i, %05.0i, %05.0i, %05.0i, %05.0i, %05.0i, %05.0i, %05.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%05.0i, %05.0i, %05.0i, %05.0i, %05.0i, %05.0i, %05.0i, %05.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%010.i, %010.i, %010.i, %010.i, %010.i, %010.i, %010.i, %010.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%010.i, %010.i, %010.i, %010.i, %010.i, %010.i, %010.i, %010.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%010.0i, %010.0i, %010.0i, %010.0i, %010.0i, %010.0i, %010.0i, %010.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%010.0i, %010.0i, %010.0i, %010.0i, %010.0i, %010.0i, %010.0i, %010.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0100.i, %0100.i, %0100.i, %0100.i, %0100.i, %0100.i, %0100.i, %0100.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0100.i, %0100.i, %0100.i, %0100.i, %0100.i, %0100.i, %0100.i, %0100.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0100.0i, %0100.0i, %0100.0i, %0100.0i, %0100.0i, %0100.0i, %0100.0i, %0100.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0100.0i, %0100.0i, %0100.0i, %0100.0i, %0100.0i, %0100.0i, %0100.0i, %0100.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+.i, %0+.i, %0+.i, %0+.i, %0+.i, %0+.i, %0+.i, %0+.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+.i, %0+.i, %0+.i, %0+.i, %0+.i, %0+.i, %0+.i, %0+.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+.0i, %0+.0i, %0+.0i, %0+.0i, %0+.0i, %0+.0i, %0+.0i, %0+.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+.0i, %0+.0i, %0+.0i, %0+.0i, %0+.0i, %0+.0i, %0+.0i, %0+.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+1.i, %0+1.i, %0+1.i, %0+1.i, %0+1.i, %0+1.i, %0+1.i, %0+1.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+1.i, %0+1.i, %0+1.i, %0+1.i, %0+1.i, %0+1.i, %0+1.i, %0+1.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+1.0i, %0+1.0i, %0+1.0i, %0+1.0i, %0+1.0i, %0+1.0i, %0+1.0i, %0+1.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+1.0i, %0+1.0i, %0+1.0i, %0+1.0i, %0+1.0i, %0+1.0i, %0+1.0i, %0+1.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+5.i, %0+5.i, %0+5.i, %0+5.i, %0+5.i, %0+5.i, %0+5.i, %0+5.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+5.i, %0+5.i, %0+5.i, %0+5.i, %0+5.i, %0+5.i, %0+5.i, %0+5.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+5.0i, %0+5.0i, %0+5.0i, %0+5.0i, %0+5.0i, %0+5.0i, %0+5.0i, %0+5.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+5.0i, %0+5.0i, %0+5.0i, %0+5.0i, %0+5.0i, %0+5.0i, %0+5.0i, %0+5.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+10.i, %0+10.i, %0+10.i, %0+10.i, %0+10.i, %0+10.i, %0+10.i, %0+10.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+10.i, %0+10.i, %0+10.i, %0+10.i, %0+10.i, %0+10.i, %0+10.i, %0+10.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+10.0i, %0+10.0i, %0+10.0i, %0+10.0i, %0+10.0i, %0+10.0i, %0+10.0i, %0+10.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+10.0i, %0+10.0i, %0+10.0i, %0+10.0i, %0+10.0i, %0+10.0i, %0+10.0i, %0+10.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+100.i, %0+100.i, %0+100.i, %0+100.i, %0+100.i, %0+100.i, %0+100.i, %0+100.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+100.i, %0+100.i, %0+100.i, %0+100.i, %0+100.i, %0+100.i, %0+100.i, %0+100.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+100.0i, %0+100.0i, %0+100.0i, %0+100.0i, %0+100.0i, %0+100.0i, %0+100.0i, %0+100.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0+100.0i, %0+100.0i, %0+100.0i, %0+100.0i, %0+100.0i, %0+100.0i, %0+100.0i, %0+100.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 .i, %0 .i, %0 .i, %0 .i, %0 .i, %0 .i, %0 .i, %0 .i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 .i, %0 .i, %0 .i, %0 .i, %0 .i, %0 .i, %0 .i, %0 .i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 .0i, %0 .0i, %0 .0i, %0 .0i, %0 .0i, %0 .0i, %0 .0i, %0 .0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 .0i, %0 .0i, %0 .0i, %0 .0i, %0 .0i, %0 .0i, %0 .0i, %0 .0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 1.i, %0 1.i, %0 1.i, %0 1.i, %0 1.i, %0 1.i, %0 1.i, %0 1.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 1.i, %0 1.i, %0 1.i, %0 1.i, %0 1.i, %0 1.i, %0 1.i, %0 1.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 1.0i, %0 1.0i, %0 1.0i, %0 1.0i, %0 1.0i, %0 1.0i, %0 1.0i, %0 1.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 1.0i, %0 1.0i, %0 1.0i, %0 1.0i, %0 1.0i, %0 1.0i, %0 1.0i, %0 1.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 5.i, %0 5.i, %0 5.i, %0 5.i, %0 5.i, %0 5.i, %0 5.i, %0 5.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 5.i, %0 5.i, %0 5.i, %0 5.i, %0 5.i, %0 5.i, %0 5.i, %0 5.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 5.0i, %0 5.0i, %0 5.0i, %0 5.0i, %0 5.0i, %0 5.0i, %0 5.0i, %0 5.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 5.0i, %0 5.0i, %0 5.0i, %0 5.0i, %0 5.0i, %0 5.0i, %0 5.0i, %0 5.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 10.i, %0 10.i, %0 10.i, %0 10.i, %0 10.i, %0 10.i, %0 10.i, %0 10.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 10.i, %0 10.i, %0 10.i, %0 10.i, %0 10.i, %0 10.i, %0 10.i, %0 10.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 10.0i, %0 10.0i, %0 10.0i, %0 10.0i, %0 10.0i, %0 10.0i, %0 10.0i, %0 10.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 10.0i, %0 10.0i, %0 10.0i, %0 10.0i, %0 10.0i, %0 10.0i, %0 10.0i, %0 10.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 100.i, %0 100.i, %0 100.i, %0 100.i, %0 100.i, %0 100.i, %0 100.i, %0 100.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 100.i, %0 100.i, %0 100.i, %0 100.i, %0 100.i, %0 100.i, %0 100.i, %0 100.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 100.0i, %0 100.0i, %0 100.0i, %0 100.0i, %0 100.0i, %0 100.0i, %0 100.0i, %0 100.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%0 100.0i, %0 100.0i, %0 100.0i, %0 100.0i, %0 100.0i, %0 100.0i, %0 100.0i, %0 100.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-1.i, %-1.i, %-1.i, %-1.i, %-1.i, %-1.i, %-1.i, %-1.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-1.i, %-1.i, %-1.i, %-1.i, %-1.i, %-1.i, %-1.i, %-1.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-1.0i, %-1.0i, %-1.0i, %-1.0i, %-1.0i, %-1.0i, %-1.0i, %-1.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-1.0i, %-1.0i, %-1.0i, %-1.0i, %-1.0i, %-1.0i, %-1.0i, %-1.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-5.i, %-5.i, %-5.i, %-5.i, %-5.i, %-5.i, %-5.i, %-5.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-5.i, %-5.i, %-5.i, %-5.i, %-5.i, %-5.i, %-5.i, %-5.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-5.0i, %-5.0i, %-5.0i, %-5.0i, %-5.0i, %-5.0i, %-5.0i, %-5.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-5.0i, %-5.0i, %-5.0i, %-5.0i, %-5.0i, %-5.0i, %-5.0i, %-5.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-10.i, %-10.i, %-10.i, %-10.i, %-10.i, %-10.i, %-10.i, %-10.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-10.i, %-10.i, %-10.i, %-10.i, %-10.i, %-10.i, %-10.i, %-10.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-10.0i, %-10.0i, %-10.0i, %-10.0i, %-10.0i, %-10.0i, %-10.0i, %-10.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-10.0i, %-10.0i, %-10.0i, %-10.0i, %-10.0i, %-10.0i, %-10.0i, %-10.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-100.i, %-100.i, %-100.i, %-100.i, %-100.i, %-100.i, %-100.i, %-100.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-100.i, %-100.i, %-100.i, %-100.i, %-100.i, %-100.i, %-100.i, %-100.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-100.0i, %-100.0i, %-100.0i, %-100.0i, %-100.0i, %-100.0i, %-100.0i, %-100.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-100.0i, %-100.0i, %-100.0i, %-100.0i, %-100.0i, %-100.0i, %-100.0i, %-100.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+.i, %-+.i, %-+.i, %-+.i, %-+.i, %-+.i, %-+.i, %-+.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+.i, %-+.i, %-+.i, %-+.i, %-+.i, %-+.i, %-+.i, %-+.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+.0i, %-+.0i, %-+.0i, %-+.0i, %-+.0i, %-+.0i, %-+.0i, %-+.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+.0i, %-+.0i, %-+.0i, %-+.0i, %-+.0i, %-+.0i, %-+.0i, %-+.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+1.i, %-+1.i, %-+1.i, %-+1.i, %-+1.i, %-+1.i, %-+1.i, %-+1.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+1.i, %-+1.i, %-+1.i, %-+1.i, %-+1.i, %-+1.i, %-+1.i, %-+1.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+1.0i, %-+1.0i, %-+1.0i, %-+1.0i, %-+1.0i, %-+1.0i, %-+1.0i, %-+1.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+1.0i, %-+1.0i, %-+1.0i, %-+1.0i, %-+1.0i, %-+1.0i, %-+1.0i, %-+1.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+5.i, %-+5.i, %-+5.i, %-+5.i, %-+5.i, %-+5.i, %-+5.i, %-+5.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+5.i, %-+5.i, %-+5.i, %-+5.i, %-+5.i, %-+5.i, %-+5.i, %-+5.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+5.0i, %-+5.0i, %-+5.0i, %-+5.0i, %-+5.0i, %-+5.0i, %-+5.0i, %-+5.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+5.0i, %-+5.0i, %-+5.0i, %-+5.0i, %-+5.0i, %-+5.0i, %-+5.0i, %-+5.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+10.i, %-+10.i, %-+10.i, %-+10.i, %-+10.i, %-+10.i, %-+10.i, %-+10.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+10.i, %-+10.i, %-+10.i, %-+10.i, %-+10.i, %-+10.i, %-+10.i, %-+10.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+10.0i, %-+10.0i, %-+10.0i, %-+10.0i, %-+10.0i, %-+10.0i, %-+10.0i, %-+10.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+10.0i, %-+10.0i, %-+10.0i, %-+10.0i, %-+10.0i, %-+10.0i, %-+10.0i, %-+10.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+100.i, %-+100.i, %-+100.i, %-+100.i, %-+100.i, %-+100.i, %-+100.i, %-+100.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+100.i, %-+100.i, %-+100.i, %-+100.i, %-+100.i, %-+100.i, %-+100.i, %-+100.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+100.0i, %-+100.0i, %-+100.0i, %-+100.0i, %-+100.0i, %-+100.0i, %-+100.0i, %-+100.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%-+100.0i, %-+100.0i, %-+100.0i, %-+100.0i, %-+100.0i, %-+100.0i, %-+100.0i, %-+100.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- .i, %- .i, %- .i, %- .i, %- .i, %- .i, %- .i, %- .i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- .i, %- .i, %- .i, %- .i, %- .i, %- .i, %- .i, %- .i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- .0i, %- .0i, %- .0i, %- .0i, %- .0i, %- .0i, %- .0i, %- .0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- .0i, %- .0i, %- .0i, %- .0i, %- .0i, %- .0i, %- .0i, %- .0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 1.i, %- 1.i, %- 1.i, %- 1.i, %- 1.i, %- 1.i, %- 1.i, %- 1.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 1.i, %- 1.i, %- 1.i, %- 1.i, %- 1.i, %- 1.i, %- 1.i, %- 1.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 1.0i, %- 1.0i, %- 1.0i, %- 1.0i, %- 1.0i, %- 1.0i, %- 1.0i, %- 1.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 1.0i, %- 1.0i, %- 1.0i, %- 1.0i, %- 1.0i, %- 1.0i, %- 1.0i, %- 1.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 5.i, %- 5.i, %- 5.i, %- 5.i, %- 5.i, %- 5.i, %- 5.i, %- 5.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 5.i, %- 5.i, %- 5.i, %- 5.i, %- 5.i, %- 5.i, %- 5.i, %- 5.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 5.0i, %- 5.0i, %- 5.0i, %- 5.0i, %- 5.0i, %- 5.0i, %- 5.0i, %- 5.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 5.0i, %- 5.0i, %- 5.0i, %- 5.0i, %- 5.0i, %- 5.0i, %- 5.0i, %- 5.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 10.i, %- 10.i, %- 10.i, %- 10.i, %- 10.i, %- 10.i, %- 10.i, %- 10.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 10.i, %- 10.i, %- 10.i, %- 10.i, %- 10.i, %- 10.i, %- 10.i, %- 10.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 10.0i, %- 10.0i, %- 10.0i, %- 10.0i, %- 10.0i, %- 10.0i, %- 10.0i, %- 10.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 10.0i, %- 10.0i, %- 10.0i, %- 10.0i, %- 10.0i, %- 10.0i, %- 10.0i, %- 10.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 100.i, %- 100.i, %- 100.i, %- 100.i, %- 100.i, %- 100.i, %- 100.i, %- 100.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 100.i, %- 100.i, %- 100.i, %- 100.i, %- 100.i, %- 100.i, %- 100.i, %- 100.i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 100.0i, %- 100.0i, %- 100.0i, %- 100.0i, %- 100.0i, %- 100.0i, %- 100.0i, %- 100.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%- 100.0i, %- 100.0i, %- 100.0i, %- 100.0i, %- 100.0i, %- 100.0i, %- 100.0i, %- 100.0i", 0, 5, -1, -10, 100, -1862, INT_MIN, INT_MAX);
	compare("%1.u, %1.u, %1.u, %1.u, %1.u, %1.u, %1.u, %1.u, %1.u, %1.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%1.u, %1.u, %1.u, %1.u, %1.u, %1.u, %1.u, %1.u, %1.u, %1.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%1.0u, %1.0u, %1.0u, %1.0u, %1.0u, %1.0u, %1.0u, %1.0u, %1.0u, %1.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%1.0u, %1.0u, %1.0u, %1.0u, %1.0u, %1.0u, %1.0u, %1.0u, %1.0u, %1.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%5.u, %5.u, %5.u, %5.u, %5.u, %5.u, %5.u, %5.u, %5.u, %5.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%5.u, %5.u, %5.u, %5.u, %5.u, %5.u, %5.u, %5.u, %5.u, %5.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%5.0u, %5.0u, %5.0u, %5.0u, %5.0u, %5.0u, %5.0u, %5.0u, %5.0u, %5.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%5.0u, %5.0u, %5.0u, %5.0u, %5.0u, %5.0u, %5.0u, %5.0u, %5.0u, %5.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%10.u, %10.u, %10.u, %10.u, %10.u, %10.u, %10.u, %10.u, %10.u, %10.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%10.u, %10.u, %10.u, %10.u, %10.u, %10.u, %10.u, %10.u, %10.u, %10.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%10.0u, %10.0u, %10.0u, %10.0u, %10.0u, %10.0u, %10.0u, %10.0u, %10.0u, %10.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%10.0u, %10.0u, %10.0u, %10.0u, %10.0u, %10.0u, %10.0u, %10.0u, %10.0u, %10.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%100.u, %100.u, %100.u, %100.u, %100.u, %100.u, %100.u, %100.u, %100.u, %100.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%100.u, %100.u, %100.u, %100.u, %100.u, %100.u, %100.u, %100.u, %100.u, %100.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%100.0u, %100.0u, %100.0u, %100.0u, %100.0u, %100.0u, %100.0u, %100.0u, %100.0u, %100.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%100.0u, %100.0u, %100.0u, %100.0u, %100.0u, %100.0u, %100.0u, %100.0u, %100.0u, %100.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%01.u, %01.u, %01.u, %01.u, %01.u, %01.u, %01.u, %01.u, %01.u, %01.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%01.u, %01.u, %01.u, %01.u, %01.u, %01.u, %01.u, %01.u, %01.u, %01.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%01.0u, %01.0u, %01.0u, %01.0u, %01.0u, %01.0u, %01.0u, %01.0u, %01.0u, %01.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%01.0u, %01.0u, %01.0u, %01.0u, %01.0u, %01.0u, %01.0u, %01.0u, %01.0u, %01.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%05.u, %05.u, %05.u, %05.u, %05.u, %05.u, %05.u, %05.u, %05.u, %05.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%05.u, %05.u, %05.u, %05.u, %05.u, %05.u, %05.u, %05.u, %05.u, %05.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%05.0u, %05.0u, %05.0u, %05.0u, %05.0u, %05.0u, %05.0u, %05.0u, %05.0u, %05.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%05.0u, %05.0u, %05.0u, %05.0u, %05.0u, %05.0u, %05.0u, %05.0u, %05.0u, %05.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%010.u, %010.u, %010.u, %010.u, %010.u, %010.u, %010.u, %010.u, %010.u, %010.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%010.u, %010.u, %010.u, %010.u, %010.u, %010.u, %010.u, %010.u, %010.u, %010.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%010.0u, %010.0u, %010.0u, %010.0u, %010.0u, %010.0u, %010.0u, %010.0u, %010.0u, %010.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%010.0u, %010.0u, %010.0u, %010.0u, %010.0u, %010.0u, %010.0u, %010.0u, %010.0u, %010.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0100.u, %0100.u, %0100.u, %0100.u, %0100.u, %0100.u, %0100.u, %0100.u, %0100.u, %0100.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0100.u, %0100.u, %0100.u, %0100.u, %0100.u, %0100.u, %0100.u, %0100.u, %0100.u, %0100.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0100.0u, %0100.0u, %0100.0u, %0100.0u, %0100.0u, %0100.0u, %0100.0u, %0100.0u, %0100.0u, %0100.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0100.0u, %0100.0u, %0100.0u, %0100.0u, %0100.0u, %0100.0u, %0100.0u, %0100.0u, %0100.0u, %0100.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-1.u, %-1.u, %-1.u, %-1.u, %-1.u, %-1.u, %-1.u, %-1.u, %-1.u, %-1.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-1.u, %-1.u, %-1.u, %-1.u, %-1.u, %-1.u, %-1.u, %-1.u, %-1.u, %-1.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-1.0u, %-1.0u, %-1.0u, %-1.0u, %-1.0u, %-1.0u, %-1.0u, %-1.0u, %-1.0u, %-1.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-1.0u, %-1.0u, %-1.0u, %-1.0u, %-1.0u, %-1.0u, %-1.0u, %-1.0u, %-1.0u, %-1.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-5.u, %-5.u, %-5.u, %-5.u, %-5.u, %-5.u, %-5.u, %-5.u, %-5.u, %-5.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-5.u, %-5.u, %-5.u, %-5.u, %-5.u, %-5.u, %-5.u, %-5.u, %-5.u, %-5.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-5.0u, %-5.0u, %-5.0u, %-5.0u, %-5.0u, %-5.0u, %-5.0u, %-5.0u, %-5.0u, %-5.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-5.0u, %-5.0u, %-5.0u, %-5.0u, %-5.0u, %-5.0u, %-5.0u, %-5.0u, %-5.0u, %-5.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-10.u, %-10.u, %-10.u, %-10.u, %-10.u, %-10.u, %-10.u, %-10.u, %-10.u, %-10.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-10.u, %-10.u, %-10.u, %-10.u, %-10.u, %-10.u, %-10.u, %-10.u, %-10.u, %-10.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-10.0u, %-10.0u, %-10.0u, %-10.0u, %-10.0u, %-10.0u, %-10.0u, %-10.0u, %-10.0u, %-10.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-10.0u, %-10.0u, %-10.0u, %-10.0u, %-10.0u, %-10.0u, %-10.0u, %-10.0u, %-10.0u, %-10.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-100.u, %-100.u, %-100.u, %-100.u, %-100.u, %-100.u, %-100.u, %-100.u, %-100.u, %-100.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-100.u, %-100.u, %-100.u, %-100.u, %-100.u, %-100.u, %-100.u, %-100.u, %-100.u, %-100.u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-100.0u, %-100.0u, %-100.0u, %-100.0u, %-100.0u, %-100.0u, %-100.0u, %-100.0u, %-100.0u, %-100.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-100.0u, %-100.0u, %-100.0u, %-100.0u, %-100.0u, %-100.0u, %-100.0u, %-100.0u, %-100.0u, %-100.0u", 0, 5, -1, -10, 100, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%1.x, %1.x, %1.x, %1.x, %1.x, %1.x, %1.x, %1.x, %1.x, %1.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%1.x, %1.x, %1.x, %1.x, %1.x, %1.x, %1.x, %1.x, %1.x, %1.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%1.0x, %1.0x, %1.0x, %1.0x, %1.0x, %1.0x, %1.0x, %1.0x, %1.0x, %1.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%1.0x, %1.0x, %1.0x, %1.0x, %1.0x, %1.0x, %1.0x, %1.0x, %1.0x, %1.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%5.x, %5.x, %5.x, %5.x, %5.x, %5.x, %5.x, %5.x, %5.x, %5.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%5.x, %5.x, %5.x, %5.x, %5.x, %5.x, %5.x, %5.x, %5.x, %5.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%5.0x, %5.0x, %5.0x, %5.0x, %5.0x, %5.0x, %5.0x, %5.0x, %5.0x, %5.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%5.0x, %5.0x, %5.0x, %5.0x, %5.0x, %5.0x, %5.0x, %5.0x, %5.0x, %5.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%10.x, %10.x, %10.x, %10.x, %10.x, %10.x, %10.x, %10.x, %10.x, %10.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%10.x, %10.x, %10.x, %10.x, %10.x, %10.x, %10.x, %10.x, %10.x, %10.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%10.0x, %10.0x, %10.0x, %10.0x, %10.0x, %10.0x, %10.0x, %10.0x, %10.0x, %10.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%10.0x, %10.0x, %10.0x, %10.0x, %10.0x, %10.0x, %10.0x, %10.0x, %10.0x, %10.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%100.x, %100.x, %100.x, %100.x, %100.x, %100.x, %100.x, %100.x, %100.x, %100.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%100.x, %100.x, %100.x, %100.x, %100.x, %100.x, %100.x, %100.x, %100.x, %100.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%100.0x, %100.0x, %100.0x, %100.0x, %100.0x, %100.0x, %100.0x, %100.0x, %100.0x, %100.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%100.0x, %100.0x, %100.0x, %100.0x, %100.0x, %100.0x, %100.0x, %100.0x, %100.0x, %100.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#1.x, %#1.x, %#1.x, %#1.x, %#1.x, %#1.x, %#1.x, %#1.x, %#1.x, %#1.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#1.x, %#1.x, %#1.x, %#1.x, %#1.x, %#1.x, %#1.x, %#1.x, %#1.x, %#1.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#1.0x, %#1.0x, %#1.0x, %#1.0x, %#1.0x, %#1.0x, %#1.0x, %#1.0x, %#1.0x, %#1.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#1.0x, %#1.0x, %#1.0x, %#1.0x, %#1.0x, %#1.0x, %#1.0x, %#1.0x, %#1.0x, %#1.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#5.x, %#5.x, %#5.x, %#5.x, %#5.x, %#5.x, %#5.x, %#5.x, %#5.x, %#5.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#5.x, %#5.x, %#5.x, %#5.x, %#5.x, %#5.x, %#5.x, %#5.x, %#5.x, %#5.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#5.0x, %#5.0x, %#5.0x, %#5.0x, %#5.0x, %#5.0x, %#5.0x, %#5.0x, %#5.0x, %#5.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#5.0x, %#5.0x, %#5.0x, %#5.0x, %#5.0x, %#5.0x, %#5.0x, %#5.0x, %#5.0x, %#5.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#10.x, %#10.x, %#10.x, %#10.x, %#10.x, %#10.x, %#10.x, %#10.x, %#10.x, %#10.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#10.x, %#10.x, %#10.x, %#10.x, %#10.x, %#10.x, %#10.x, %#10.x, %#10.x, %#10.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#10.0x, %#10.0x, %#10.0x, %#10.0x, %#10.0x, %#10.0x, %#10.0x, %#10.0x, %#10.0x, %#10.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#10.0x, %#10.0x, %#10.0x, %#10.0x, %#10.0x, %#10.0x, %#10.0x, %#10.0x, %#10.0x, %#10.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#100.x, %#100.x, %#100.x, %#100.x, %#100.x, %#100.x, %#100.x, %#100.x, %#100.x, %#100.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#100.x, %#100.x, %#100.x, %#100.x, %#100.x, %#100.x, %#100.x, %#100.x, %#100.x, %#100.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#100.0x, %#100.0x, %#100.0x, %#100.0x, %#100.0x, %#100.0x, %#100.0x, %#100.0x, %#100.0x, %#100.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#100.0x, %#100.0x, %#100.0x, %#100.0x, %#100.0x, %#100.0x, %#100.0x, %#100.0x, %#100.0x, %#100.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%01.x, %01.x, %01.x, %01.x, %01.x, %01.x, %01.x, %01.x, %01.x, %01.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%01.x, %01.x, %01.x, %01.x, %01.x, %01.x, %01.x, %01.x, %01.x, %01.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%01.0x, %01.0x, %01.0x, %01.0x, %01.0x, %01.0x, %01.0x, %01.0x, %01.0x, %01.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%01.0x, %01.0x, %01.0x, %01.0x, %01.0x, %01.0x, %01.0x, %01.0x, %01.0x, %01.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%05.x, %05.x, %05.x, %05.x, %05.x, %05.x, %05.x, %05.x, %05.x, %05.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%05.x, %05.x, %05.x, %05.x, %05.x, %05.x, %05.x, %05.x, %05.x, %05.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%05.0x, %05.0x, %05.0x, %05.0x, %05.0x, %05.0x, %05.0x, %05.0x, %05.0x, %05.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%05.0x, %05.0x, %05.0x, %05.0x, %05.0x, %05.0x, %05.0x, %05.0x, %05.0x, %05.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%010.x, %010.x, %010.x, %010.x, %010.x, %010.x, %010.x, %010.x, %010.x, %010.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%010.x, %010.x, %010.x, %010.x, %010.x, %010.x, %010.x, %010.x, %010.x, %010.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%010.0x, %010.0x, %010.0x, %010.0x, %010.0x, %010.0x, %010.0x, %010.0x, %010.0x, %010.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%010.0x, %010.0x, %010.0x, %010.0x, %010.0x, %010.0x, %010.0x, %010.0x, %010.0x, %010.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0100.x, %0100.x, %0100.x, %0100.x, %0100.x, %0100.x, %0100.x, %0100.x, %0100.x, %0100.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0100.x, %0100.x, %0100.x, %0100.x, %0100.x, %0100.x, %0100.x, %0100.x, %0100.x, %0100.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0100.0x, %0100.0x, %0100.0x, %0100.0x, %0100.0x, %0100.0x, %0100.0x, %0100.0x, %0100.0x, %0100.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0100.0x, %0100.0x, %0100.0x, %0100.0x, %0100.0x, %0100.0x, %0100.0x, %0100.0x, %0100.0x, %0100.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#1.x, %0#1.x, %0#1.x, %0#1.x, %0#1.x, %0#1.x, %0#1.x, %0#1.x, %0#1.x, %0#1.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#1.x, %0#1.x, %0#1.x, %0#1.x, %0#1.x, %0#1.x, %0#1.x, %0#1.x, %0#1.x, %0#1.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#1.0x, %0#1.0x, %0#1.0x, %0#1.0x, %0#1.0x, %0#1.0x, %0#1.0x, %0#1.0x, %0#1.0x, %0#1.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#1.0x, %0#1.0x, %0#1.0x, %0#1.0x, %0#1.0x, %0#1.0x, %0#1.0x, %0#1.0x, %0#1.0x, %0#1.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#5.x, %0#5.x, %0#5.x, %0#5.x, %0#5.x, %0#5.x, %0#5.x, %0#5.x, %0#5.x, %0#5.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#5.x, %0#5.x, %0#5.x, %0#5.x, %0#5.x, %0#5.x, %0#5.x, %0#5.x, %0#5.x, %0#5.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#5.0x, %0#5.0x, %0#5.0x, %0#5.0x, %0#5.0x, %0#5.0x, %0#5.0x, %0#5.0x, %0#5.0x, %0#5.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#5.0x, %0#5.0x, %0#5.0x, %0#5.0x, %0#5.0x, %0#5.0x, %0#5.0x, %0#5.0x, %0#5.0x, %0#5.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#10.x, %0#10.x, %0#10.x, %0#10.x, %0#10.x, %0#10.x, %0#10.x, %0#10.x, %0#10.x, %0#10.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#10.x, %0#10.x, %0#10.x, %0#10.x, %0#10.x, %0#10.x, %0#10.x, %0#10.x, %0#10.x, %0#10.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#10.0x, %0#10.0x, %0#10.0x, %0#10.0x, %0#10.0x, %0#10.0x, %0#10.0x, %0#10.0x, %0#10.0x, %0#10.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#10.0x, %0#10.0x, %0#10.0x, %0#10.0x, %0#10.0x, %0#10.0x, %0#10.0x, %0#10.0x, %0#10.0x, %0#10.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#100.x, %0#100.x, %0#100.x, %0#100.x, %0#100.x, %0#100.x, %0#100.x, %0#100.x, %0#100.x, %0#100.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#100.x, %0#100.x, %0#100.x, %0#100.x, %0#100.x, %0#100.x, %0#100.x, %0#100.x, %0#100.x, %0#100.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-1.x, %-1.x, %-1.x, %-1.x, %-1.x, %-1.x, %-1.x, %-1.x, %-1.x, %-1.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-1.x, %-1.x, %-1.x, %-1.x, %-1.x, %-1.x, %-1.x, %-1.x, %-1.x, %-1.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-1.0x, %-1.0x, %-1.0x, %-1.0x, %-1.0x, %-1.0x, %-1.0x, %-1.0x, %-1.0x, %-1.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-1.0x, %-1.0x, %-1.0x, %-1.0x, %-1.0x, %-1.0x, %-1.0x, %-1.0x, %-1.0x, %-1.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-5.x, %-5.x, %-5.x, %-5.x, %-5.x, %-5.x, %-5.x, %-5.x, %-5.x, %-5.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-5.x, %-5.x, %-5.x, %-5.x, %-5.x, %-5.x, %-5.x, %-5.x, %-5.x, %-5.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-5.0x, %-5.0x, %-5.0x, %-5.0x, %-5.0x, %-5.0x, %-5.0x, %-5.0x, %-5.0x, %-5.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-5.0x, %-5.0x, %-5.0x, %-5.0x, %-5.0x, %-5.0x, %-5.0x, %-5.0x, %-5.0x, %-5.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-10.x, %-10.x, %-10.x, %-10.x, %-10.x, %-10.x, %-10.x, %-10.x, %-10.x, %-10.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-10.x, %-10.x, %-10.x, %-10.x, %-10.x, %-10.x, %-10.x, %-10.x, %-10.x, %-10.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-10.0x, %-10.0x, %-10.0x, %-10.0x, %-10.0x, %-10.0x, %-10.0x, %-10.0x, %-10.0x, %-10.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-10.0x, %-10.0x, %-10.0x, %-10.0x, %-10.0x, %-10.0x, %-10.0x, %-10.0x, %-10.0x, %-10.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-100.x, %-100.x, %-100.x, %-100.x, %-100.x, %-100.x, %-100.x, %-100.x, %-100.x, %-100.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-100.x, %-100.x, %-100.x, %-100.x, %-100.x, %-100.x, %-100.x, %-100.x, %-100.x, %-100.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-100.0x, %-100.0x, %-100.0x, %-100.0x, %-100.0x, %-100.0x, %-100.0x, %-100.0x, %-100.0x, %-100.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-100.0x, %-100.0x, %-100.0x, %-100.0x, %-100.0x, %-100.0x, %-100.0x, %-100.0x, %-100.0x, %-100.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#1.x, %-#1.x, %-#1.x, %-#1.x, %-#1.x, %-#1.x, %-#1.x, %-#1.x, %-#1.x, %-#1.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#1.x, %-#1.x, %-#1.x, %-#1.x, %-#1.x, %-#1.x, %-#1.x, %-#1.x, %-#1.x, %-#1.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#1.0x, %-#1.0x, %-#1.0x, %-#1.0x, %-#1.0x, %-#1.0x, %-#1.0x, %-#1.0x, %-#1.0x, %-#1.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#1.0x, %-#1.0x, %-#1.0x, %-#1.0x, %-#1.0x, %-#1.0x, %-#1.0x, %-#1.0x, %-#1.0x, %-#1.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#5.x, %-#5.x, %-#5.x, %-#5.x, %-#5.x, %-#5.x, %-#5.x, %-#5.x, %-#5.x, %-#5.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#5.x, %-#5.x, %-#5.x, %-#5.x, %-#5.x, %-#5.x, %-#5.x, %-#5.x, %-#5.x, %-#5.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#5.0x, %-#5.0x, %-#5.0x, %-#5.0x, %-#5.0x, %-#5.0x, %-#5.0x, %-#5.0x, %-#5.0x, %-#5.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#5.0x, %-#5.0x, %-#5.0x, %-#5.0x, %-#5.0x, %-#5.0x, %-#5.0x, %-#5.0x, %-#5.0x, %-#5.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#10.x, %-#10.x, %-#10.x, %-#10.x, %-#10.x, %-#10.x, %-#10.x, %-#10.x, %-#10.x, %-#10.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#10.x, %-#10.x, %-#10.x, %-#10.x, %-#10.x, %-#10.x, %-#10.x, %-#10.x, %-#10.x, %-#10.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#10.0x, %-#10.0x, %-#10.0x, %-#10.0x, %-#10.0x, %-#10.0x, %-#10.0x, %-#10.0x, %-#10.0x, %-#10.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#10.0x, %-#10.0x, %-#10.0x, %-#10.0x, %-#10.0x, %-#10.0x, %-#10.0x, %-#10.0x, %-#10.0x, %-#10.0x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#100.x, %-#100.x, %-#100.x, %-#100.x, %-#100.x, %-#100.x, %-#100.x, %-#100.x, %-#100.x, %-#100.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#100.x, %-#100.x, %-#100.x, %-#100.x, %-#100.x, %-#100.x, %-#100.x, %-#100.x, %-#100.x, %-#100.x", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%1.X, %1.X, %1.X, %1.X, %1.X, %1.X, %1.X, %1.X, %1.X, %1.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%1.X, %1.X, %1.X, %1.X, %1.X, %1.X, %1.X, %1.X, %1.X, %1.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%1.0X, %1.0X, %1.0X, %1.0X, %1.0X, %1.0X, %1.0X, %1.0X, %1.0X, %1.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%1.0X, %1.0X, %1.0X, %1.0X, %1.0X, %1.0X, %1.0X, %1.0X, %1.0X, %1.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%5.X, %5.X, %5.X, %5.X, %5.X, %5.X, %5.X, %5.X, %5.X, %5.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%5.X, %5.X, %5.X, %5.X, %5.X, %5.X, %5.X, %5.X, %5.X, %5.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%5.0X, %5.0X, %5.0X, %5.0X, %5.0X, %5.0X, %5.0X, %5.0X, %5.0X, %5.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%5.0X, %5.0X, %5.0X, %5.0X, %5.0X, %5.0X, %5.0X, %5.0X, %5.0X, %5.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%10.X, %10.X, %10.X, %10.X, %10.X, %10.X, %10.X, %10.X, %10.X, %10.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%10.X, %10.X, %10.X, %10.X, %10.X, %10.X, %10.X, %10.X, %10.X, %10.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%10.0X, %10.0X, %10.0X, %10.0X, %10.0X, %10.0X, %10.0X, %10.0X, %10.0X, %10.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%10.0X, %10.0X, %10.0X, %10.0X, %10.0X, %10.0X, %10.0X, %10.0X, %10.0X, %10.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%100.X, %100.X, %100.X, %100.X, %100.X, %100.X, %100.X, %100.X, %100.X, %100.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%100.X, %100.X, %100.X, %100.X, %100.X, %100.X, %100.X, %100.X, %100.X, %100.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%100.0X, %100.0X, %100.0X, %100.0X, %100.0X, %100.0X, %100.0X, %100.0X, %100.0X, %100.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%100.0X, %100.0X, %100.0X, %100.0X, %100.0X, %100.0X, %100.0X, %100.0X, %100.0X, %100.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#1.X, %#1.X, %#1.X, %#1.X, %#1.X, %#1.X, %#1.X, %#1.X, %#1.X, %#1.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#1.X, %#1.X, %#1.X, %#1.X, %#1.X, %#1.X, %#1.X, %#1.X, %#1.X, %#1.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#1.0X, %#1.0X, %#1.0X, %#1.0X, %#1.0X, %#1.0X, %#1.0X, %#1.0X, %#1.0X, %#1.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#1.0X, %#1.0X, %#1.0X, %#1.0X, %#1.0X, %#1.0X, %#1.0X, %#1.0X, %#1.0X, %#1.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#5.X, %#5.X, %#5.X, %#5.X, %#5.X, %#5.X, %#5.X, %#5.X, %#5.X, %#5.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#5.X, %#5.X, %#5.X, %#5.X, %#5.X, %#5.X, %#5.X, %#5.X, %#5.X, %#5.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#5.0X, %#5.0X, %#5.0X, %#5.0X, %#5.0X, %#5.0X, %#5.0X, %#5.0X, %#5.0X, %#5.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#5.0X, %#5.0X, %#5.0X, %#5.0X, %#5.0X, %#5.0X, %#5.0X, %#5.0X, %#5.0X, %#5.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#10.X, %#10.X, %#10.X, %#10.X, %#10.X, %#10.X, %#10.X, %#10.X, %#10.X, %#10.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#10.X, %#10.X, %#10.X, %#10.X, %#10.X, %#10.X, %#10.X, %#10.X, %#10.X, %#10.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#10.0X, %#10.0X, %#10.0X, %#10.0X, %#10.0X, %#10.0X, %#10.0X, %#10.0X, %#10.0X, %#10.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#10.0X, %#10.0X, %#10.0X, %#10.0X, %#10.0X, %#10.0X, %#10.0X, %#10.0X, %#10.0X, %#10.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#100.X, %#100.X, %#100.X, %#100.X, %#100.X, %#100.X, %#100.X, %#100.X, %#100.X, %#100.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#100.X, %#100.X, %#100.X, %#100.X, %#100.X, %#100.X, %#100.X, %#100.X, %#100.X, %#100.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#100.0X, %#100.0X, %#100.0X, %#100.0X, %#100.0X, %#100.0X, %#100.0X, %#100.0X, %#100.0X, %#100.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%#100.0X, %#100.0X, %#100.0X, %#100.0X, %#100.0X, %#100.0X, %#100.0X, %#100.0X, %#100.0X, %#100.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%01.X, %01.X, %01.X, %01.X, %01.X, %01.X, %01.X, %01.X, %01.X, %01.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%01.X, %01.X, %01.X, %01.X, %01.X, %01.X, %01.X, %01.X, %01.X, %01.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%01.0X, %01.0X, %01.0X, %01.0X, %01.0X, %01.0X, %01.0X, %01.0X, %01.0X, %01.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%01.0X, %01.0X, %01.0X, %01.0X, %01.0X, %01.0X, %01.0X, %01.0X, %01.0X, %01.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%05.X, %05.X, %05.X, %05.X, %05.X, %05.X, %05.X, %05.X, %05.X, %05.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%05.X, %05.X, %05.X, %05.X, %05.X, %05.X, %05.X, %05.X, %05.X, %05.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%05.0X, %05.0X, %05.0X, %05.0X, %05.0X, %05.0X, %05.0X, %05.0X, %05.0X, %05.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%05.0X, %05.0X, %05.0X, %05.0X, %05.0X, %05.0X, %05.0X, %05.0X, %05.0X, %05.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%010.X, %010.X, %010.X, %010.X, %010.X, %010.X, %010.X, %010.X, %010.X, %010.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%010.X, %010.X, %010.X, %010.X, %010.X, %010.X, %010.X, %010.X, %010.X, %010.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%010.0X, %010.0X, %010.0X, %010.0X, %010.0X, %010.0X, %010.0X, %010.0X, %010.0X, %010.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%010.0X, %010.0X, %010.0X, %010.0X, %010.0X, %010.0X, %010.0X, %010.0X, %010.0X, %010.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0100.X, %0100.X, %0100.X, %0100.X, %0100.X, %0100.X, %0100.X, %0100.X, %0100.X, %0100.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0100.X, %0100.X, %0100.X, %0100.X, %0100.X, %0100.X, %0100.X, %0100.X, %0100.X, %0100.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0100.0X, %0100.0X, %0100.0X, %0100.0X, %0100.0X, %0100.0X, %0100.0X, %0100.0X, %0100.0X, %0100.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0100.0X, %0100.0X, %0100.0X, %0100.0X, %0100.0X, %0100.0X, %0100.0X, %0100.0X, %0100.0X, %0100.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#1.X, %0#1.X, %0#1.X, %0#1.X, %0#1.X, %0#1.X, %0#1.X, %0#1.X, %0#1.X, %0#1.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#1.X, %0#1.X, %0#1.X, %0#1.X, %0#1.X, %0#1.X, %0#1.X, %0#1.X, %0#1.X, %0#1.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#1.0X, %0#1.0X, %0#1.0X, %0#1.0X, %0#1.0X, %0#1.0X, %0#1.0X, %0#1.0X, %0#1.0X, %0#1.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#1.0X, %0#1.0X, %0#1.0X, %0#1.0X, %0#1.0X, %0#1.0X, %0#1.0X, %0#1.0X, %0#1.0X, %0#1.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#5.X, %0#5.X, %0#5.X, %0#5.X, %0#5.X, %0#5.X, %0#5.X, %0#5.X, %0#5.X, %0#5.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#5.X, %0#5.X, %0#5.X, %0#5.X, %0#5.X, %0#5.X, %0#5.X, %0#5.X, %0#5.X, %0#5.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#5.0X, %0#5.0X, %0#5.0X, %0#5.0X, %0#5.0X, %0#5.0X, %0#5.0X, %0#5.0X, %0#5.0X, %0#5.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#5.0X, %0#5.0X, %0#5.0X, %0#5.0X, %0#5.0X, %0#5.0X, %0#5.0X, %0#5.0X, %0#5.0X, %0#5.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#10.X, %0#10.X, %0#10.X, %0#10.X, %0#10.X, %0#10.X, %0#10.X, %0#10.X, %0#10.X, %0#10.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#10.X, %0#10.X, %0#10.X, %0#10.X, %0#10.X, %0#10.X, %0#10.X, %0#10.X, %0#10.X, %0#10.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#10.0X, %0#10.0X, %0#10.0X, %0#10.0X, %0#10.0X, %0#10.0X, %0#10.0X, %0#10.0X, %0#10.0X, %0#10.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#10.0X, %0#10.0X, %0#10.0X, %0#10.0X, %0#10.0X, %0#10.0X, %0#10.0X, %0#10.0X, %0#10.0X, %0#10.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#100.X, %0#100.X, %0#100.X, %0#100.X, %0#100.X, %0#100.X, %0#100.X, %0#100.X, %0#100.X, %0#100.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%0#100.X, %0#100.X, %0#100.X, %0#100.X, %0#100.X, %0#100.X, %0#100.X, %0#100.X, %0#100.X, %0#100.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-1.X, %-1.X, %-1.X, %-1.X, %-1.X, %-1.X, %-1.X, %-1.X, %-1.X, %-1.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-1.X, %-1.X, %-1.X, %-1.X, %-1.X, %-1.X, %-1.X, %-1.X, %-1.X, %-1.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-1.0X, %-1.0X, %-1.0X, %-1.0X, %-1.0X, %-1.0X, %-1.0X, %-1.0X, %-1.0X, %-1.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-1.0X, %-1.0X, %-1.0X, %-1.0X, %-1.0X, %-1.0X, %-1.0X, %-1.0X, %-1.0X, %-1.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-5.X, %-5.X, %-5.X, %-5.X, %-5.X, %-5.X, %-5.X, %-5.X, %-5.X, %-5.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-5.X, %-5.X, %-5.X, %-5.X, %-5.X, %-5.X, %-5.X, %-5.X, %-5.X, %-5.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-5.0X, %-5.0X, %-5.0X, %-5.0X, %-5.0X, %-5.0X, %-5.0X, %-5.0X, %-5.0X, %-5.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-5.0X, %-5.0X, %-5.0X, %-5.0X, %-5.0X, %-5.0X, %-5.0X, %-5.0X, %-5.0X, %-5.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-10.X, %-10.X, %-10.X, %-10.X, %-10.X, %-10.X, %-10.X, %-10.X, %-10.X, %-10.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-10.X, %-10.X, %-10.X, %-10.X, %-10.X, %-10.X, %-10.X, %-10.X, %-10.X, %-10.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-10.0X, %-10.0X, %-10.0X, %-10.0X, %-10.0X, %-10.0X, %-10.0X, %-10.0X, %-10.0X, %-10.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-10.0X, %-10.0X, %-10.0X, %-10.0X, %-10.0X, %-10.0X, %-10.0X, %-10.0X, %-10.0X, %-10.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-100.X, %-100.X, %-100.X, %-100.X, %-100.X, %-100.X, %-100.X, %-100.X, %-100.X, %-100.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-100.X, %-100.X, %-100.X, %-100.X, %-100.X, %-100.X, %-100.X, %-100.X, %-100.X, %-100.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-100.0X, %-100.0X, %-100.0X, %-100.0X, %-100.0X, %-100.0X, %-100.0X, %-100.0X, %-100.0X, %-100.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-100.0X, %-100.0X, %-100.0X, %-100.0X, %-100.0X, %-100.0X, %-100.0X, %-100.0X, %-100.0X, %-100.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#1.X, %-#1.X, %-#1.X, %-#1.X, %-#1.X, %-#1.X, %-#1.X, %-#1.X, %-#1.X, %-#1.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#1.X, %-#1.X, %-#1.X, %-#1.X, %-#1.X, %-#1.X, %-#1.X, %-#1.X, %-#1.X, %-#1.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#1.0X, %-#1.0X, %-#1.0X, %-#1.0X, %-#1.0X, %-#1.0X, %-#1.0X, %-#1.0X, %-#1.0X, %-#1.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#1.0X, %-#1.0X, %-#1.0X, %-#1.0X, %-#1.0X, %-#1.0X, %-#1.0X, %-#1.0X, %-#1.0X, %-#1.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#5.X, %-#5.X, %-#5.X, %-#5.X, %-#5.X, %-#5.X, %-#5.X, %-#5.X, %-#5.X, %-#5.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#5.X, %-#5.X, %-#5.X, %-#5.X, %-#5.X, %-#5.X, %-#5.X, %-#5.X, %-#5.X, %-#5.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#5.0X, %-#5.0X, %-#5.0X, %-#5.0X, %-#5.0X, %-#5.0X, %-#5.0X, %-#5.0X, %-#5.0X, %-#5.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#5.0X, %-#5.0X, %-#5.0X, %-#5.0X, %-#5.0X, %-#5.0X, %-#5.0X, %-#5.0X, %-#5.0X, %-#5.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#10.X, %-#10.X, %-#10.X, %-#10.X, %-#10.X, %-#10.X, %-#10.X, %-#10.X, %-#10.X, %-#10.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#10.X, %-#10.X, %-#10.X, %-#10.X, %-#10.X, %-#10.X, %-#10.X, %-#10.X, %-#10.X, %-#10.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#10.0X, %-#10.0X, %-#10.0X, %-#10.0X, %-#10.0X, %-#10.0X, %-#10.0X, %-#10.0X, %-#10.0X, %-#10.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#10.0X, %-#10.0X, %-#10.0X, %-#10.0X, %-#10.0X, %-#10.0X, %-#10.0X, %-#10.0X, %-#10.0X, %-#10.0X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#100.X, %-#100.X, %-#100.X, %-#100.X, %-#100.X, %-#100.X, %-#100.X, %-#100.X, %-#100.X, %-#100.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);
	compare("%-#100.X, %-#100.X, %-#100.X, %-#100.X, %-#100.X, %-#100.X, %-#100.X, %-#100.X, %-#100.X, %-#100.X", 0, 5, -1, -10, 0x1234, -1862, 0xABCDE, INT_MIN, INT_MAX, UINT_MAX);

#endif // ndef NO_BONUS

#endif

	fputs("\n===\n", stderr);
	return (finish(EXIT_SUCCESS));
}
