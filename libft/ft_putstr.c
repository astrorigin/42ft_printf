/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/07 22:25:34 by pmarquis          #+#    #+#             */
/*   Updated: 2022/11/07 22:27:57 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_putstr(char *s, int fd)
{
	while (*s)
	{
		if (write(fd, s++, 1) != 1)
			return (0);
	}
	return (1);
}

int	ft_putstr_sz(char *s, size_t sz, int fd)
{
	size_t	i;

	i = 0;
	while (i < sz)
	{
		if (!ft_putchar(s[i++], fd))
			return (0);
	}
	return (1);
}
